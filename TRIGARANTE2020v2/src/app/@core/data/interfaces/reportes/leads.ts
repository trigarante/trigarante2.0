export interface Leads {
  idSolictud: number;
  fechaSolicitud: string;
  nombreCliente: string;
  telefono: number;
  correo: string;
  codigoPostal: string;
  marca: string;
  modelo: string;
  descripcion: string;
  detalle: string;
  campana: string;
  nombreEmpleado: string;
  ulrs: string;
  estadoContacto: string;
  status: string;
}
