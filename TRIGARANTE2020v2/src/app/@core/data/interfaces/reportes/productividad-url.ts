export interface ProductividadUrl {
  idMedioDifusion: number;
  urlCompleta: string;
  contactos: number;
  polizasEmitidas: number;
  primaNetaEmitida: number;
  polizasCobradas: number;
  primaNetaCobrada: number;
  polizasAplicadas: number;
  primaNetaAplicada: number;
}
