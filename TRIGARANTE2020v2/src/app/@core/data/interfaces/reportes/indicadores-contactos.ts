export interface IndicadoresContactos {
    url: string;
    contactos: number;
    tipoSubarea: string;
    leadsMaximos: number;
}
