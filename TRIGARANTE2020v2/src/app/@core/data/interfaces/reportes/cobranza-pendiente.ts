export interface CobranzaPendiente {
  id: number;
  poliza: string;
  fechaRegistro: string;
  inicioVigencia: string;
  compania: string;
  numeroSerie: string;
  nombreCliente: string;
  prima: number;
  temporalidad: string;
  estatus: string;
  aplicacionFinanzas: string;
  idEmpleado: number;
  ejecutivo: string;
  diferenciaDias: string;
  area: string;
  campana: string;
}
