export interface ProductividadEjecutivo {
  idEmpleado: string;
  turno: string;
  tipoSubarea: string;
  nombreEjecutivo: string;
  antiguedad: number;
  estado: number;
  sede: string;
  idSede: number;
  contactos: number;
  polizasEmitidas: number;
  primaNetaEmitida: number;
  polizasCobradas: number;
  primaNetaCobrada: number;
  polizasAplicadas: number;
  primaNetaAplicada: number;
}
