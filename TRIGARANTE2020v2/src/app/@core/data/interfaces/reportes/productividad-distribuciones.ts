export interface ProductividadDistribuciones {
  nombreComercial: string;
  polizasEmitidas: number;
  primaNetaEmitida: number;
  polizasCobradas: number;
  primaNetaCobrada: number;
  polizasAplicadas: number;
  primaNetaAplicada: number;
}
