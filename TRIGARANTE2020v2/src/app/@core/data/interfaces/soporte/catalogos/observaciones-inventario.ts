import {Observable} from 'rxjs';

export interface ObservacionesInventario {
  id: number;
  observaciones: string;
  activo: number;
}
export abstract class ObservacionesInventarioData {
  abstract get(): Observable<ObservacionesInventario[]>;
  abstract post(observacionesInventario): Observable<ObservacionesInventario>;
  abstract postSocket(json): Observable<JSON>;
  abstract getObservacionesInventario(idObservacionesInventario): Observable<ObservacionesInventario>;
  abstract put(idObservacionesInventario, observacionesInventario): Observable<ObservacionesInventario>;
}
