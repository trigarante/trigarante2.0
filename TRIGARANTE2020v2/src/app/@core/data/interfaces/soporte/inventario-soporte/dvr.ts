import {Observable} from 'rxjs';

export interface Dvr {
  id: number;
  idEmpleado: number;
  idObservacionesInventario: number;
  idEstadoInventario: number;
  folio: string;
  idMarca: number;
  idTipoSede: number;
  origenRecurso: string;
  activo: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  nombreMarca: string;
  estadoInventario?: string;
  observaciones?: string;
  sedeNombre?: string;
}

export abstract class DvrData {
  abstract get(): Observable<Dvr[]>;

  abstract post(dvr): Observable<Dvr>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;

  abstract getDvrById(idDvr): Observable<Dvr>;

  abstract put(idDvr, dvr): Observable<Dvr>;
}
