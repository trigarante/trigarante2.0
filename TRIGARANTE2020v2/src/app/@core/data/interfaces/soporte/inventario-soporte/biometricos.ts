import {Observable} from 'rxjs';

export interface Biometricos {
  id: number;
  idEmpleado: number;
  idObservacionesInventario: number;
  idEstadoInventario: number;
  folio: string;
  idMarca: number;
  idTipoSede: number;
  origenRecurso: string;
  activo: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  nombreMarca: string;
  estadoInventario?: string;
  observaciones?: string;
  sedeNombre?: string;
}

export abstract class BiometricosData {

  abstract get(): Observable<Biometricos[]>;

  abstract post(biometricos): Observable<Biometricos>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;

  abstract getBiometricosById(idBiometricos): Observable<Biometricos>;

  abstract put(idBiometricos, biometricos): Observable<Biometricos>;
}
