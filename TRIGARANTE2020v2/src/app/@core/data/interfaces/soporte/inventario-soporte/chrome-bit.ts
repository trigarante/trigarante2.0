import {Observable} from 'rxjs';

export interface ChromeBit {
  id: number;
  idEmpleado: number;
  idObservacionesInventario: number;
  idEstadoInventario: number;
  folio: string;
  idMarca: number;
  idTipoSede: number;
  origenRecurso: string;
  activo: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  nombreMarca: string;
  estadoInventario?: string;
  observaciones?: string;
  sedeNombre?: string;
}

export abstract class ChromeBitData {

  abstract get(): Observable<ChromeBit[]>;

  abstract post(chromeBit): Observable<ChromeBit>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;

  abstract getChromeBit(idChromeBit): Observable<ChromeBit>;

  abstract put(idChromeBit, chromeBit): Observable<ChromeBit>;
}
