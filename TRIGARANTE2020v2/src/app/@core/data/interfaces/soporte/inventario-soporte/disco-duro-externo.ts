import {Observable} from 'rxjs';

export interface DiscoDuroExterno {
  id: number;
  idEmpleado: number;
  idObservacionesInventario: number;
  idEstadoInventario: number;
  folio: string;
  idMarca: number;
  idTipoSede: number;
  origenRecurso: string;
  activo: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  nombreMarca: string;
  estadoInventario?: string;
  observaciones?: string;
  sedeNombre?: string;
}

export abstract class DiscoDuroExternoData {
  abstract get(): Observable<DiscoDuroExterno[]>;

  abstract post(discoDuroExterno): Observable<DiscoDuroExterno>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;

  abstract getDiscoExternoById(idDiscoExterno): Observable<DiscoDuroExterno>;

  abstract put(idDiscoExterno, discoDuroExterno): Observable<DiscoDuroExterno>;
}
