import {Observable} from 'rxjs';

export interface Tablet {
  id: number;
  idEmpleado?: number;
  numFolio: string;
  idProcesador: string;
  capacidad: string;
  ram: string;
  idMarca: string;
  idEstadoInventario: number;
  telefono: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  idObservacionesInventario: number;
  estadoInventario?: string;
  observacionesInventario?: string;
  comentarios: string;
  activo: number;
  nombreMarca: string;
  nombreProcesador: string;
  idTipoSede?: number;
  origenRecurso?: string;
  sedeNombre?: string;
}
export abstract class TabletData {
  abstract get(): Observable<Tablet[]>;
  abstract post(tablet): Observable<Tablet>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;
  abstract getTabletById(idTablet): Observable<Tablet>;
  abstract put(idTablet, tablet): Observable<Tablet>;
}
