import {Observable} from 'rxjs';
export interface Diadema {
  id: number;
  idEmpleado?: number;
  numFolio: string;
  idMarca: number;
  idEstadoInventario: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  idObservacionesInventario: number;
  estadoInventario?: string;
  observacionesInventario?: string;
  comentarios: string;
  activo: number;
  nombreMarca: string;
  idTipoSede?: number;
  origenRecurso?: string;
  sedeNombre?: string;
}

export abstract class DiademaData {
  abstract get(): Observable<Diadema[]>;

  abstract post(diadema): Observable<Diadema>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;

  abstract getDiademaById(idDiadema): Observable<Diadema>;

  abstract put(idDiadema, diadema): Observable<Diadema>;
}
