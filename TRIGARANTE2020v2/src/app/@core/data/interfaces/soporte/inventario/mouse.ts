import {Observable} from 'rxjs';

export interface Mouse {
  id: number;
  idEmpleado?: number;
  idMarca: number;
  modelo: string;
  idEstadoInventario: number;
  fechaRecepcion: Date;
  fechaSalida: Date;
  idObservacionesInventario: number;
  estadoInventario?: string;
  observacionesInventario?: string;
  comentarios: string;
  activo: number;
  nombreMarca?: string;
  idTipoSede?: number;
  origenRecurso?: string;
  sedeNombre?: string;
}
export abstract class MouseData {
  abstract get(): Observable<Mouse[]>;
  abstract post(mouse): Observable<Mouse>;
  abstract postSocket(jsno): Observable<JSON>;
  abstract postSocketBaja(jsno): Observable<JSON>;
  abstract getMouseById(idMouse): Observable<Mouse>;
  abstract put(idMouse, mouse): Observable<Mouse>;
}
