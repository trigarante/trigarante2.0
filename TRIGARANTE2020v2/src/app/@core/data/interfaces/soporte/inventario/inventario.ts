import {Observable} from 'rxjs';

export class Inventario {
  id?: number;
  idEmpleado: number;
  idEquipo: number;
  idTabla: number;
  fechaAsignacion: Date;
  fechaLiberacion?: Date;
  activo: number;
}

export abstract class InventarioData {
  abstract get(): Observable<Inventario[]>;

  abstract post(inventario: Inventario): Observable<Inventario>;
}
