import {Observable} from 'rxjs';
import {Empleados} from '../rrhh/empelados/empleados';

export interface PreBaja {
  id: number;
  idEmpleado: number;
  fechaBaja: Date;
  fechaRegistro: Date;
  comentario: string;
  idPrecandidato: number;
  activo: number;
  idEmpleadoSolicitante: number;
}

export abstract class PreBajaData {
  abstract getAllPreBaja(): Observable<PreBaja[]>;
  abstract getAllPreBajaView(): Observable<PreBaja[]>;
  abstract getPreBajaById(idEmpleado: number): Observable<PreBaja>;
  abstract getfiltradoPreBaja(idSubarea: number): Observable<Empleados[]>;
  abstract postPreBaja(empleado): Observable<PreBaja>;
  abstract putPreBaja(idEmpleado, empleado): Observable<PreBaja>;
}
