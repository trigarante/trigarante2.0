import {Observable} from 'rxjs';

export interface CampanaSubarea {
  id: number;
  idCampana: number;
  idSubarea: number;
  descripcionn: string;
  activo: number;
  nombre?: string;
  subarea?: string;
}
export abstract class CampanaSubareaData {
  abstract get(): Observable<CampanaSubarea[]>;

  abstract post(campanaSubarea): Observable<CampanaSubarea>;

  abstract getCampanaSubareaById(idCampanaSubarea): Observable<CampanaSubarea>;

  abstract put(idCampanaSubarea, campanaSubarea): Observable<CampanaSubarea>;

}

