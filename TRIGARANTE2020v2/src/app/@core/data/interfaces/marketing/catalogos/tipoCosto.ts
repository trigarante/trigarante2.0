import {Observable} from 'rxjs';
import {Paises} from "../../catalogos/paises";

export interface TipoCosto {
  id: number;
  tipo: string;
  objetivo: string;
  activo: number;
}
export abstract class TipoCostoData {
  abstract get(): Observable<TipoCosto[]>;

  abstract post(tipoCosto): Observable<TipoCosto>;

  abstract getByActivo(activo): Observable<TipoCosto[]>;

  abstract getTipoCostoById(idTipoCosto): Observable<TipoCosto>;

  abstract put(idTipoCosto, tipoCosto): Observable<TipoCosto>;
}
