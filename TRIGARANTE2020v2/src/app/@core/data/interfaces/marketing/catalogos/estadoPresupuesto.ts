import {Observable} from 'rxjs';

export interface EstadoPresupuesto {
  id: number;
  estado: string;
  objetivo: string;
  activo: number;
}
export abstract class EstadoPresupuestoData {
  abstract get(): Observable<EstadoPresupuesto[]>;

  abstract post(estadoPresupueto): Observable<EstadoPresupuesto>;

  abstract getByActivo(activo): Observable<EstadoPresupuesto[]>;

  abstract getEstadoPresupuestoById(idEstadoPresupuesto): Observable<EstadoPresupuesto>;

  abstract put(idEstadoPresupuesto, estadoPresupuesto): Observable<EstadoPresupuesto>;
}
