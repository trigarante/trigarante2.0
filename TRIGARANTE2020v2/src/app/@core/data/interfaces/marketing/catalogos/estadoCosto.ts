import {Observable} from 'rxjs';

export interface EstadoCosto {
  id: number;
  estado: string;
  objetivo: string;
  activo: number;
}
export abstract class EstadoCostoData {
  abstract get(): Observable<EstadoCosto[]>;

  abstract getByActivo(activo): Observable<EstadoCosto[]>;

  abstract post(estadoCosto): Observable<EstadoCosto>;

  abstract getEstadoCostoById(idEstadoCosto): Observable<EstadoCosto>;

  abstract put(idEstadoCosto, estadoCosto): Observable<EstadoCosto>;
}
