import {Observable} from 'rxjs';


export interface TipoPresupuesto {
  id: number;
  tipo: string;
  objetivo: string;
  activo: number;
}
export abstract class TipoPresupuestoData {
  abstract get(): Observable<TipoPresupuesto[]>;

  abstract getByActivo(activo): Observable<TipoPresupuesto[]>;

  abstract post(tipoPresupuesto): Observable<TipoPresupuesto>;

  abstract getTipoPresupuestoById(idTipoPresupuesto): Observable<TipoPresupuesto>;

  abstract put(idTipoPresupuesto, tipoPresupuesto): Observable<TipoPresupuesto>;
}
