import {Observable} from 'rxjs';
import {Paises} from "../../catalogos/paises";

export interface EstadoCampana {
  id: number;
  estado: string;
  activo: number;
}
export abstract class EstadoCampanaData {
  abstract get(): Observable<EstadoCampana[]>;

  abstract post(estadoCampana): Observable<EstadoCampana>;

  abstract getByActivo(activo): Observable<EstadoCampana[]>;

  abstract getEstadoCampanaById(idEstadoCampana): Observable<EstadoCampana>;

  abstract put(idEstadoCampana, estadoCampana): Observable<EstadoCampana>;
}
