import {Observable} from 'rxjs';


export interface PeriodoTiempo {
  id: number;
  periodo: string;
  objetivo: string;
  activo: number;
}
export abstract class PeriodoTiempoData {
  abstract get(): Observable<PeriodoTiempo[]>;

  abstract post(periodoTiempo): Observable<PeriodoTiempo>;

  abstract getPeriodoTiempoById(idPeriodoTiempo): Observable<PeriodoTiempo>;

  abstract put(idPeriodoTiempo, periodoTiempo): Observable<PeriodoTiempo>;

  abstract getByActivo(activo): Observable<PeriodoTiempo[]>;
}
