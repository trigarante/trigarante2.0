import {Observable} from 'rxjs';

export interface CampanaMarketing {
  id: number;
  idSubRamo: number;
  nombre: string;
  descripcion: string;
  activo: number;
  idEstadoCampana: number;
  idMarcaComercial: number;
  estado?: string;
  // Datos de la vista
  idRamo?: number;
  idSocio?: number;
}

export abstract class CampanaMarketingData {
  abstract get(): Observable<CampanaMarketing[]>;

  abstract post(campanaMarketing): Observable<CampanaMarketing>;

  abstract postSocket(json): Observable<JSON>;

  abstract getCampanasMarketingBiId(idCampanaMarketing): Observable<CampanaMarketing>;

  abstract put(idCampanaMarketing, campanaMarketing): Observable<CampanaMarketing>;
}
