import {Observable} from 'rxjs';

export interface CampanaCategoria {
  idCampana: number;
  activo: number;
  comentarios: string;
  nombre?: string;
  idSubRamo: number;
  prioridad?: number;
  descripcion?: string;
}

export abstract class CampanaCategoriaData {
  abstract get(): Observable<CampanaCategoria[]>;

  abstract post(campanaCategoria): Observable<CampanaCategoria>;
  abstract postSocket(json): Observable<JSON>;

  abstract getCampanasCategoriaById(idCampanaCategoria): Observable<CampanaCategoria>;

  abstract put(idCampanaCategoria, campanaCategoria): Observable<CampanaCategoria>;
}
