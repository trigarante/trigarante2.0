import {Observable} from 'rxjs';

export interface PresupuestoCampana {
  id: number;
  idCampana: number;
  idPeriodoTiempo: number;
  idProveedorLead: number;
  idEstadoPresupuesto: number;
  idTipoPresupuesto: number;
  fecha: string;
  fechaRegistro: string;
  cantidad: number;
  activo: number;
  nombre?: string;
  periodo?: string;
  nombreproveedorLeads?: string;
  estado?: string;
  tipo?: string;
}
export abstract class PresupuestoCampanaData {
  abstract get(): Observable<PresupuestoCampana[]>;

  abstract post(presupuestoCampana): Observable<PresupuestoCampana>;

  abstract getPresupuestoCampanaById(idPresupuestoCampana): Observable<PresupuestoCampana>;

  abstract put(idPresupuestoCampana, presupestoCampana): Observable<PresupuestoCampana>;
}
