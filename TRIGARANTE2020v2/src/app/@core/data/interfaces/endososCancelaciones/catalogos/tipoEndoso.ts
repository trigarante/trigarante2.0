import {Observable} from 'rxjs';

export interface TipoEndoso {
  id: number;
  descripcion: string;
  activo: number;

}

export abstract class TipoEndosoData {
  abstract get(): Observable<TipoEndoso[]>;

  abstract post(tipoEndoso: TipoEndoso): Observable<TipoEndoso>;

  abstract getTipoEndosoById(idTipoEndoso): Observable<TipoEndoso>;

  abstract put(idTipoEndoso, tipoEndoso): Observable<TipoEndoso>;
}
