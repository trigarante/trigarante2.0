import {Observable} from 'rxjs';
export interface MotivoEndoso {
  id: number;
  descripcion: string;
  activo: number;

}

export abstract class MotivoEndosoData {
  abstract get(): Observable<MotivoEndoso[]>;
  abstract post(tipoEndoso: MotivoEndoso): Observable<MotivoEndoso>;
  abstract getMotivoEndosoById(idMotivoEndoso): Observable<MotivoEndoso>;
  abstract getByIdTipoEndoso(idMotivoEndoso: number): Observable<MotivoEndoso[]>;
  abstract put(idTipoEndoso, tipoEndoso): Observable<MotivoEndoso>;
}

