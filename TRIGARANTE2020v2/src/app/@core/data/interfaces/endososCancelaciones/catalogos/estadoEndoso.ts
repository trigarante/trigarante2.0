import {Observable} from 'rxjs';

export interface EstadoEndoso {
  id: number;
  descripcion: string;
  activo: number;

}

export abstract class EstadoEndosoData {
  abstract get(): Observable<EstadoEndoso[]>;
  abstract post(estadoEndoso: EstadoEndoso): Observable<EstadoEndoso>;
  abstract getEstadoEndosoById(idEstadoEndoso): Observable<EstadoEndoso>;
  abstract put(idEstadoEndoso, estadoEndoso): Observable<EstadoEndoso>;
}
