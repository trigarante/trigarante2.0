import {Observable} from 'rxjs';

export interface EstadoSolicitudEndoso {
  id: number;
  descripcion: string;
  activo: number;

}

export abstract class EstadoSolicitudEndosoData {
  abstract get(): Observable<EstadoSolicitudEndoso[]>;

  abstract post(estadoSolicitudEndoso: EstadoSolicitudEndoso): Observable<EstadoSolicitudEndoso>;

  abstract getEstadoSolicitudEndosoById(idEstadoSolicitudEndoso): Observable<EstadoSolicitudEndoso>;

  abstract put(idEstadoSolicitudEndoso, estadoSolicitudEndoso): Observable<EstadoSolicitudEndoso>;
}
