import {Observable} from 'rxjs';

export interface EndososCancelaciones {
  id: number;
  idSolicitudEndoso: number;
  idTipoEndoso: number;
  idEstadoEndoso: number;
  fechaEmision: string;
  fechaAplicacion: string;
  prima: number;
  comentarios: string;
  activo: number;
  idRegistro?: number;
  idEstadoSolicitudEndoso?: number;
  solicitudEndosoComentarios?: number;
  tipoEndososDescripcion?: number;
  descripcion?: number;

}

export abstract class EndososCancelacionesData {
  abstract get(): Observable<EndososCancelaciones[]>;

  abstract post(endososCancelaciones: EndososCancelaciones): Observable<EndososCancelaciones>;

  abstract getEndososCancelacionesById(idEndososCancelaciones): Observable<EndososCancelaciones>;

  abstract put(idEndososCancelaciones, endososCancelaciones): Observable<EndososCancelaciones>;
}
