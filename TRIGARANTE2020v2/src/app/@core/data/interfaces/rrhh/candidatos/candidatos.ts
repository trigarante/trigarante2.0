import {Observable} from 'rxjs';

export interface Candidatos {
  id: number;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  calificacionPsicometrico: number;
  calificacionExamen: number;
  idPrecandidato: number;
  comentarios: string;
  fechaCreacion: string;
  fechaIngreso: string;
  fechaBaja: number;
  idEstadoRH: number;
  idEtapa: number;
  telefonoMovil: number;
  idTipoUsuario: number;
  sede: number;
  idPuesto: number;
  puesto: string;
}


