import {Observable} from 'rxjs';

export interface Precandidatos {
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  curp: string;
  estadoRh: string;
  genero: string;
  fechaNacimiento: Date;
  idPais: number;
  idEstadoCivil: number;
  idEstadoRH: number;
  idEscolaridad: number;
  idEstadoEscolaridad: number;
  idSolicitudRrhh: number;
  email: string;
  cp: number;
  colonia: string;
  idColonia: number;
  calle: string;
  numeroInterior: number;
  numeroExterior: number;
  telefonoFijo: number;
  telefonoMovil: number;
  fechaCreacion: string;
  fechaBaja: string;
  tiempoTraslado: number;
  idMedioTraslado: number;
  idEstacion: number;
  idEstadoRRHH: number;
  idEtapa: number;
  detalleBaja: string;
  nombreEtapa: string;
  recontratable: number;
  recontratables: string;
  sede: string;
  idSede: number;
  idSubarea: number;
  subarea: string;
  // activo:number;
  //  estado:string;
  //  delMun:string;
  //  ciudad:string;
}


