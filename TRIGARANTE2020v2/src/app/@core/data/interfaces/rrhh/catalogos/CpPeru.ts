import {Observable} from 'rxjs';

export interface CpPeru {
  id: number;
  cp: number;
  capitales: string;
  distritos: string;
  provincia: string;
  departamento: string;
}

export abstract class CpPeruData {
  abstract getCapitalesById(cp: number): Observable<CpPeru>;
  abstract getCapitales(id: number): Observable<CpPeru[]>;
}
