import { Observable } from 'rxjs';


export interface TipoContactoVn {
  id: number;
  descripcion: string;
  activo: number;
}

export abstract class TipoContactoVnData {
  abstract get(): Observable<TipoContactoVn[]>;

  abstract post(contacto): Observable<TipoContactoVn>;
  abstract postSocket(json): Observable<JSON>;

  abstract getContactoById(idContacto): Observable<TipoContactoVn>;

  abstract put(idContacto, contacto): Observable<TipoContactoVn>;
}
