import {Observable} from 'rxjs';

export interface TipoPuesto {
  id: number;
  nombre: string;
  activo: number;
}
export abstract class TipoPuestoData {
  abstract get(): Observable<TipoPuesto[]>;
  abstract post(TipoPuesto): Observable<TipoPuesto>;
  abstract postSocket(json): Observable<JSON>;
  abstract put(idTipoPuesto, TipoPuesto): Observable<TipoPuesto>;
  abstract getTipoPuestoById(idTipoPuesto): Observable<TipoPuesto>;
  abstract getByActivo(activo): Observable<TipoPuesto[]>;


}
