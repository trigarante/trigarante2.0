import {Observable} from 'rxjs';

export interface Solicitudes {
    nombre: string;
    apellidoPaterno: string;
    apellidoMaterno: string;
    telefono: number;
    correo: string;
    cp: number;
    edad: number;
    empleado: string;
    estado: any;
    fechaCita: Date;
    idBolsaTrabajo: number;
    bolsaTrabajo: string;
    idVacante: number;
    vacante: string;
    documentos: string;
    fechaDescartado: Date;
    idSede: number;
    sede: string;
    idEstadoRH: number;
    motivo: string;
    idReclutador: number;
    comentarios: string;
    idEtapa: number;
}

