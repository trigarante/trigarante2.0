import {Observable} from 'rxjs';

export interface GestionIngresos {
  id: number;
  idSubarea: number;
  idTipoIngreso: number;
  cantidad: number;
  fecha?: string;
  fechaRegistro: string;
  activo: number;
  descripcion?: string;
  ingresos?: number;
  subarea?: number;

}

export abstract class GestionIngresosData {
  abstract get(): Observable<GestionIngresos[]>;

  abstract post(gestionIngresos: GestionIngresos): Observable<GestionIngresos>;

  abstract getGestionIngresosById(idGestionIngresos): Observable<GestionIngresos>;

  abstract put(idGestionIngresos, gestionIngresos): Observable<GestionIngresos>;
}
