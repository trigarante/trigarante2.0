import {Observable} from 'rxjs';

export interface TipoEndosoFinanzas {
  id: number;
  tipoEndoso: string;
  activo: number;
}

export abstract class TipoEndosoFinanzasData {
  abstract get(): Observable<TipoEndosoFinanzas[]>;
  abstract post(tipoEndososFinanzas): Observable<TipoEndosoFinanzas>;
  abstract getTipoEndosoFinanzasById(idTipoEndosoFinanzas): Observable<TipoEndosoFinanzas>;
  abstract put(idTipoEndosoFinanzas, tipoEndososFinanzas): Observable<TipoEndosoFinanzas>;
}
