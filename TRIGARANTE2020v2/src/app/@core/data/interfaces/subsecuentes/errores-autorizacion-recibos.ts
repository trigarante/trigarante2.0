import {Observable} from 'rxjs';

export interface ErroresAutorizacionRecibos {
  id: number;
  idAutorizacionRecibo: number;
  idEmpleado: number;
  idEstadoCorreccion: number;
  idTipoDocumento: number;
  idEmpleadoCorreccion: number;
  correcciones: string;
}

export abstract class ErroresAutorizacionRecibosData {
  abstract get(): Observable<ErroresAutorizacionRecibos[]>;
  abstract post(camposConErrores: ErroresAutorizacionRecibos): Observable<string>;
  abstract getCamposConErrores(idAutorizacionRegistro: number, idTipoDocumento: number): Observable<ErroresAutorizacionRecibos>;
  abstract putEstadoCorreccion(idErrorAutorizacion: number): Observable<any>;
}

