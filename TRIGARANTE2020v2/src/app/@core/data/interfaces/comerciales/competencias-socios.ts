import {Observable} from 'rxjs';

export interface CompetenciasSocios {
  idProducto: number;
  idTipoCompetencia: number;
  competencia: string;
  activo: number;
  idEstado: number;
  idEstadoSocio: number;
  alias: any;
  descripcion: any;
  estado: any;
  nombre: any;
  nombreComercial: any;
  tipoCategoria: any;
  tipoCompetencia: any;
  tipoGiro: any;
  tipoRamo: string;
  tipoSubRamo: string;
}

export abstract class CompetenciaSocioData {
  abstract get(): Observable<CompetenciasSocios[]>;

  abstract post(competenciaSocios): Observable<CompetenciasSocios>;
  abstract postSocket(json): Observable<JSON>;

  abstract getCompetenciaSociosById(idCompetenciaSocios): Observable<CompetenciasSocios>;

  abstract put(idCompetenciaSocios, competenciaSocios): Observable<CompetenciasSocios>;
}
