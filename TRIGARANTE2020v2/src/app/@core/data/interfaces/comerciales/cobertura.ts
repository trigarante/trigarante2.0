import {Observable} from 'rxjs';

export interface Cobertura {
  cobertura: string;
  activo: number;
  alias: any;
  descripcion: any;
  detalle: any;
  estado: any;
  nombre: any;
  nombreComercial: any;
  tipoCategoria: any;
  tipoCobertura: any;
  tipoGiro: any;
  idTipoCobertura: number;
  id: number;
  idProducto: number;
  tipoRamo: string;
  tipoSubRamo: string;
}

export abstract class CoberturaData {
  abstract get(): Observable<Cobertura[]>;

  abstract post(cobertura): Observable<Cobertura>;
  abstract postSocket(json): Observable<JSON>;

  abstract getCoberturaById(idCobertura): Observable<Cobertura>;

  abstract put(idCobertura, cobertura): Observable<Cobertura>;
}
