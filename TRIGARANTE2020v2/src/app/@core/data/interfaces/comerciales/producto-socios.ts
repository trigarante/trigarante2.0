import {Observable} from 'rxjs';

export interface ProductoSocios {
  idSubRamo: number;
  idCobertura: number;
  prioridad: number;
  nombre: string;
  activo: number;
  idTipoProducto: number;
  idEstado: number;
  idEstadoSocio: number;
  alias: any;
  descripcion: any;
  estado: any;
  nombreComercial: any;
  prioridades: any;
  tipoSubRamo: any;
  tipoRamo: any;
  tipoProducto: any;
  id: number;
}

export abstract class ProductoData {
  abstract get(): Observable<ProductoSocios[]>;
  abstract getProductoSociosById(idProductoSocios): Observable<ProductoSocios>;
  abstract getProductoSocioByIdSubRamo(idSubramo: number): Observable<ProductoSocios[]>;
  abstract post(productoSocios): Observable<ProductoSocios>;
  abstract postSocket(json): Observable<JSON>;
  abstract put(idProductoSocios, productoSocios): Observable<ProductoSocios>;
}
