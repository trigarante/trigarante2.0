import {Observable} from 'rxjs';

export interface TipoTickets {
  id: number;
  descripcion: string;
  activo: number;
}

export abstract class TipoTicketsData {
  abstract  get(): Observable<TipoTickets[]>;
  abstract getByActivo(activo: number): Observable<TipoTickets[]>;
  abstract  getById(idTipoTicket: number): Observable<TipoTickets>;
  abstract post(tipoTicket): Observable<TipoTickets>;
  abstract put(idTipoTicket: number, tipoTicket: TipoTickets): Observable<TipoTickets>;
}
