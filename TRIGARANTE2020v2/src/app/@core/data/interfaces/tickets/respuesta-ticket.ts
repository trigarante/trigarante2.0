import {Observable} from "rxjs";

export interface RespuestaTicket {
  id: number;
  descripcion: string;
  activo: number;
}

export abstract class RespuestaTicketData {
  abstract get(): Observable<RespuestaTicket[]>;
  abstract getById(idRespuestaTicket: number): Observable<RespuestaTicket>;
  abstract getByActivo(activo: number): Observable<RespuestaTicket[]>;
  abstract post(respuestaticket): Observable<RespuestaTicket>;
  abstract  put(idRespuestaTicket, respuestaticket): Observable<RespuestaTicket>;
}
