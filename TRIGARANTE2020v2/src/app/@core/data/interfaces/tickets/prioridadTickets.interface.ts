import {Observable, ObservableInput} from 'rxjs';

export interface PrioridadTickets {
  id: number;
  descripcion: string;
  activo: number;
}

export abstract  class PrioridadTicketsData {
  abstract  get(): Observable<PrioridadTickets[]>;
  abstract  getById(idPrioridad: number): Observable<PrioridadTickets>;
  abstract  getByActivo(activo: number): Observable<PrioridadTickets[]>;
  abstract  post(prioridadTicket): Observable<PrioridadTickets>;
  abstract  put(idPrioridad: number, prioridadTicket: PrioridadTickets): Observable<PrioridadTickets>;
}
