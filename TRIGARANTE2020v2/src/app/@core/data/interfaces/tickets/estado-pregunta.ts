import {Observable} from 'rxjs';

export interface EstadoPregunta {
  id: number;
  descripcion: string;
  activo: number;
}

export abstract class EstadoPreguntaData {
  abstract get(): Observable<EstadoPregunta[]>;
  abstract getById(idEstadoPregunta: number): Observable<EstadoPregunta>;
  abstract  getByActivo(activo: number): Observable<EstadoPregunta[]>;
  abstract post(estadoPRegunta): Observable<EstadoPregunta>;
  abstract  put(idEstadoPregunta, estadoPregunta): Observable<EstadoPregunta>;
}
