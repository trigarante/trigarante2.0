import {Observable} from 'rxjs';

export interface Tickets {
  id: number;
  idEmpleado: number;
  idSubarea: number;
  idTipoTicket: number;
  idEstadoTicket: number;
  fechaSolicitud: any;
  idMotivoTicket: number;
  fechaCierre?: string;
  comentarios: string;
  activo: number;
  nombre?: string;
  apellidoPaterno?: string;
  apellidoMaterno?: string;
  subarea?: string;
  estadoTicketDescripcion?: string;
  motivosTicketsDescripcion?: string;
  tipoTicketDescripcion?: string;
  idEmpleadoReceptor?: number;
  horaSolicitud?: string;
  horaCierre?: string;
  idCalificacionTicket?: number;
  archivo?: string;
}

export  abstract class TicketsData {
  abstract get(): Observable<Tickets[]>;
  abstract getById(idTicket: number): Observable<Tickets>;
  abstract  getViewById(idTicke: number): Observable<Tickets>;
  abstract getByIdEstadoTicket(idEstadoTicket: number): Observable<Tickets[]>;
  abstract getByIdEmpleadoAndIdEstadoTicket(idEmpleado: number, idEstadoTicket: number): Observable<Tickets[]>;
  abstract  getview(): Observable<Tickets[]>;
  abstract post(ticket): Observable<Tickets>;
  abstract  put(idTicket, ticket): Observable<Tickets>;
}
