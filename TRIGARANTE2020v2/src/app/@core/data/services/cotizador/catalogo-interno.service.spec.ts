import { TestBed } from '@angular/core/testing';

import { CatalogoInternoService } from './catalogo-interno.service';

describe('CatalogoInternoService', () => {
  let service: CatalogoInternoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CatalogoInternoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
