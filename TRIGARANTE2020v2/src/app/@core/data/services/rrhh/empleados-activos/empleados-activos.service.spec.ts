import { TestBed } from '@angular/core/testing';

import { EmpleadosActivosService } from './empleados-activos.service';

describe('EmpleadosActivosService', () => {
  let service: EmpleadosActivosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmpleadosActivosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
