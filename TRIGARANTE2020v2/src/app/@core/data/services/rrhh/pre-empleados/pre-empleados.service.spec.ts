import { TestBed } from '@angular/core/testing';

import { PreEmpleadosService } from './pre-empleados.service';

describe('PreEmpleadosService', () => {
  let service: PreEmpleadosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PreEmpleadosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
