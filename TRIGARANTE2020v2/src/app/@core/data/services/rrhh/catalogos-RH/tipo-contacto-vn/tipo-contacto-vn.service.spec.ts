import { TestBed } from '@angular/core/testing';

import { TipoContactoVnService } from './tipo-contacto-vn.service';

describe('TipoContactoVnService', () => {
  let service: TipoContactoVnService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoContactoVnService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
