import { TestBed } from '@angular/core/testing';

import { EstadoRrhhService } from './estado-rrhh.service';

describe('EstadoRrhhService', () => {
  let service: EstadoRrhhService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EstadoRrhhService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
