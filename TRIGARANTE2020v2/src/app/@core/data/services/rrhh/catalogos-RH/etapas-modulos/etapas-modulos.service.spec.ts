import { TestBed } from '@angular/core/testing';

import { EtapasModulosService } from './etapas-modulos.service';

describe('EtapasModulosService', () => {
  let service: EtapasModulosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EtapasModulosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
