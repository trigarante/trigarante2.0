import { TestBed } from '@angular/core/testing';

import { EstadoEscolaridadService } from './estado-escolaridad.service';

describe('EstadoEscolaridadService', () => {
  let service: EstadoEscolaridadService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EstadoEscolaridadService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
