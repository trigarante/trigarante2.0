import { TestBed } from '@angular/core/testing';

import { TipoPuestoService } from './tipo-puesto.service';

describe('TipoPuestoService', () => {
  let service: TipoPuestoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoPuestoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
