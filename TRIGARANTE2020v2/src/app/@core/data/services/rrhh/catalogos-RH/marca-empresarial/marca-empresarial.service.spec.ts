import { TestBed } from '@angular/core/testing';

import { MarcaEmpresarialService } from './marca-empresarial.service';

describe('MarcaEmpresarialService', () => {
  let service: MarcaEmpresarialService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MarcaEmpresarialService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
