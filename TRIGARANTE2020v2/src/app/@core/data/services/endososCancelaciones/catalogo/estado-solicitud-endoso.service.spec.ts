import { TestBed } from '@angular/core/testing';

import { EstadoSolicitudEndosoService } from './estado-solicitud-endoso.service';

describe('EstadoSolicitudEndosoService', () => {
  let service: EstadoSolicitudEndosoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EstadoSolicitudEndosoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
