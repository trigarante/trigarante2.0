import { TestBed } from '@angular/core/testing';

import { RecibosPagoService } from './recibos-pago.service';

describe('RecibosPagoService', () => {
  let service: RecibosPagoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RecibosPagoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
