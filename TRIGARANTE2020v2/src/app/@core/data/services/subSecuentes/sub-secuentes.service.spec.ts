import { TestBed } from '@angular/core/testing';

import { SubSecuentesService } from './sub-secuentes.service';

describe('SubSecuentesService', () => {
  let service: SubSecuentesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubSecuentesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
