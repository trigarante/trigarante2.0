import { TestBed } from '@angular/core/testing';

import { AutorizacionReciboService } from './autorizacion-recibo.service';

describe('AutorizacionReciboService', () => {
  let service: AutorizacionReciboService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AutorizacionReciboService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
