import { TestBed } from '@angular/core/testing';

import { ErroresAnexosRecibosService } from './errores-anexos-recibos.service';

describe('ErroresAnexosRecibosService', () => {
  let service: ErroresAnexosRecibosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ErroresAnexosRecibosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
