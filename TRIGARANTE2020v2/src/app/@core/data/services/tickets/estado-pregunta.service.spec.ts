import { TestBed } from '@angular/core/testing';

import { EstadoPreguntaService } from './estado-pregunta.service';

describe('EstadoPreguntaService', () => {
  let service: EstadoPreguntaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EstadoPreguntaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
