import { TestBed } from '@angular/core/testing';

import { OpcionTicketsService } from './opcion-tickets.service';

describe('OpcionTicketsService', () => {
  let service: OpcionTicketsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OpcionTicketsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
