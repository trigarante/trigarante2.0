import { TestBed } from '@angular/core/testing';

import { GrupoUsuariosService } from './grupo-usuarios.service';

describe('GrupoUsuariosService', () => {
  let service: GrupoUsuariosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GrupoUsuariosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
