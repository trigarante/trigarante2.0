import { TestBed } from '@angular/core/testing';

import { VariablesGlobalService } from './variables-global.service';

describe('VariablesGlobalService', () => {
  let service: VariablesGlobalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VariablesGlobalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
