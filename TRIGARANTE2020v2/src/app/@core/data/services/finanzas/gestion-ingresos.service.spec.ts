import { TestBed } from '@angular/core/testing';

import { GestionIngresosService } from './gestion-ingresos.service';

describe('GestionIngresosService', () => {
  let service: GestionIngresosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestionIngresosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
