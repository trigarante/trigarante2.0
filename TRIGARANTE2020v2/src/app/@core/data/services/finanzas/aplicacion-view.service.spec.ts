import { TestBed } from '@angular/core/testing';

import { AplicacionViewService } from './aplicacion-view.service';

describe('AplicacionViewService', () => {
  let service: AplicacionViewService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AplicacionViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
