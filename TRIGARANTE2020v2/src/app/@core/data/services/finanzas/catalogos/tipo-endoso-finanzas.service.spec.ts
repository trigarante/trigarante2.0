import { TestBed } from '@angular/core/testing';

import { TipoEndosoFinanzasService } from './tipo-endoso-finanzas.service';

describe('TipoEndosoFinanzasService', () => {
  let service: TipoEndosoFinanzasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoEndosoFinanzasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
