import { TestBed } from '@angular/core/testing';

import { CompetenciasSociosService } from './competencias-socios.service';

describe('CompetenciasSociosService', () => {
  let service: CompetenciasSociosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CompetenciasSociosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
