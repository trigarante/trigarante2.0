import { TestBed } from '@angular/core/testing';

import { AnalisisCompetenciasService } from './analisis-competencias.service';

describe('AnalisisCompetenciasService', () => {
  let service: AnalisisCompetenciasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AnalisisCompetenciasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
