import { TestBed } from '@angular/core/testing';

import { ProductoSociosService } from './producto-socios.service';

describe('ProductoSociosService', () => {
  let service: ProductoSociosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductoSociosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
