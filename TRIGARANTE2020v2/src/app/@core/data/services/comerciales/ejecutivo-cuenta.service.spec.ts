import { TestBed } from '@angular/core/testing';

import { EjecutivoCuentaService } from './ejecutivo-cuenta.service';

describe('EjecutivoCuentaService', () => {
  let service: EjecutivoCuentaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EjecutivoCuentaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
