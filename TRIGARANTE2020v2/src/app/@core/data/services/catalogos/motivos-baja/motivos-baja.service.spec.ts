import { TestBed } from '@angular/core/testing';

import { MotivosBajaService } from './motivos-baja.service';

describe('MotivosBajaService', () => {
  let service: MotivosBajaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MotivosBajaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
