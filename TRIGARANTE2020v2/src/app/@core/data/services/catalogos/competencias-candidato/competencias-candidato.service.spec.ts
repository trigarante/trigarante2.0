import { TestBed } from '@angular/core/testing';

import { CompetenciasCandidatoService } from './competencias-candidato.service';

describe('CompetenciasCandidatoService', () => {
  let service: CompetenciasCandidatoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CompetenciasCandidatoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
