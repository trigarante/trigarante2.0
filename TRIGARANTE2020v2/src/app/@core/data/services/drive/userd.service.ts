import { Injectable } from '@angular/core';
import {Userd} from '../../interfaces/drive/userd';
const TABLE_USER = 'users';

@Injectable()
export class UserdService {
  add(profile: gapi.auth2.BasicProfile) {
    const users = this.getAll();

    let foundIndex = -1;
    for (let i = 0; i < users.length; i++) {
      if (users[i].Id === profile.getId()) {
        foundIndex = i;
        break;
      }
    }
    if (foundIndex >= 0)
      users.splice(foundIndex, 1);

    users.push(Userd.fromBasicProfile(profile));
    this.save(users);
  }

  getAll(): Userd[] {
    const data = localStorage.getItem(TABLE_USER);
    if (data) {
      return <Userd[]>(JSON.parse(data));
    } else
      return [];
  }

  save(users: Userd[]) {
    localStorage.setItem(TABLE_USER, JSON.stringify(users));
  }
}
