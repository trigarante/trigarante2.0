import { TestBed } from '@angular/core/testing';

import { TipoSiniestroService } from './tipo-siniestro.service';

describe('TipoSiniestroService', () => {
  let service: TipoSiniestroService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoSiniestroService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
