import { TestBed } from '@angular/core/testing';

import { DiscoDuroExternoService } from './disco-duro-externo.service';

describe('DiscoDuroExternoService', () => {
  let service: DiscoDuroExternoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DiscoDuroExternoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
