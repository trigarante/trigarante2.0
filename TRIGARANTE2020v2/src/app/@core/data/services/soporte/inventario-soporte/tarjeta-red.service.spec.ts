import { TestBed } from '@angular/core/testing';

import { TarjetaRedService } from './tarjeta-red.service';

describe('TarjetaRedService', () => {
  let service: TarjetaRedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TarjetaRedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
