import { TestBed } from '@angular/core/testing';

import { FuenteCamarasService } from './fuente-camaras.service';

describe('FuenteCamarasService', () => {
  let service: FuenteCamarasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FuenteCamarasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
