import { TestBed } from '@angular/core/testing';

import { PatchPanelService } from './patch-panel.service';

describe('PatchPanelService', () => {
  let service: PatchPanelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PatchPanelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
