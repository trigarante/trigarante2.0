import { TestBed } from '@angular/core/testing';

import { CharolasRackService } from './charolas-rack.service';

describe('CharolasRackService', () => {
  let service: CharolasRackService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CharolasRackService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
