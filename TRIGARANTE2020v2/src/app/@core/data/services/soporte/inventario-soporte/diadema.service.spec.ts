import { TestBed } from '@angular/core/testing';

import { DiademaService } from './diadema.service';

describe('DiademaService', () => {
  let service: DiademaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DiademaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
