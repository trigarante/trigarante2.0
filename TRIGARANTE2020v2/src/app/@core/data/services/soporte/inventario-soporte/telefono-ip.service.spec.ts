import { TestBed } from '@angular/core/testing';

import { TelefonoIpService } from './telefono-ip.service';

describe('TelefonoIpService', () => {
  let service: TelefonoIpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TelefonoIpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
