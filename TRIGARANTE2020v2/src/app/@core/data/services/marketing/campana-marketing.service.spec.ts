import { TestBed } from '@angular/core/testing';

import { CampanaMarketingService } from './campana-marketing.service';

describe('CampanaMarketingService', () => {
  let service: CampanaMarketingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampanaMarketingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
