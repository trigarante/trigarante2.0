import { TestBed } from '@angular/core/testing';

import { PresupuestoCampanaService } from './presupuesto-campana.service';

describe('PresupuestoCampanaService', () => {
  let service: PresupuestoCampanaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PresupuestoCampanaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
