import { TestBed } from '@angular/core/testing';

import { PresupuestoMarketingService } from './presupuesto-marketing.service';

describe('PresupuestoMarketingService', () => {
  let service: PresupuestoMarketingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PresupuestoMarketingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
