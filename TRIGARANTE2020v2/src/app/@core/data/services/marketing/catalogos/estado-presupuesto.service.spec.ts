import { TestBed } from '@angular/core/testing';

import { EstadoPresupuestoService } from './estado-presupuesto.service';

describe('EstadoPresupuestoService', () => {
  let service: EstadoPresupuestoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EstadoPresupuestoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
