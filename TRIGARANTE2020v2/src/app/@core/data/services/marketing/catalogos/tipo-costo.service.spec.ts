import { TestBed } from '@angular/core/testing';

import { TipoCostoService } from './tipo-costo.service';

describe('TipoCostoService', () => {
  let service: TipoCostoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoCostoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
