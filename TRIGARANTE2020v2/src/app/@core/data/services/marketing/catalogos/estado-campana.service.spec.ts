import { TestBed } from '@angular/core/testing';

import { EstadoCampanaService } from './estado-campana.service';

describe('EstadoCampanaService', () => {
  let service: EstadoCampanaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EstadoCampanaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
