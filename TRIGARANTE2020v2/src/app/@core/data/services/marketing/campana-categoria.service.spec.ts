import { TestBed } from '@angular/core/testing';

import { CampanaCategoriaService } from './campana-categoria.service';

describe('CampanaCategoriaService', () => {
  let service: CampanaCategoriaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampanaCategoriaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
