import { TestBed } from '@angular/core/testing';

import { MediosDifusionService } from './medios-difusion.service';

describe('MediosDifusionService', () => {
  let service: MediosDifusionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MediosDifusionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
