import { TestBed } from '@angular/core/testing';

import { MetasCampanaService } from './metas-campana.service';

describe('MetasCampanaService', () => {
  let service: MetasCampanaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MetasCampanaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
