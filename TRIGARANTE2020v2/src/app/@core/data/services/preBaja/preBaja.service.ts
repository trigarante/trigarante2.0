import {Injectable} from '@angular/core';
import {PreBaja, PreBajaData} from '../../interfaces/preBaja/preBaja';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable, of} from 'rxjs';
import {Empleados} from "../../interfaces/rrhh/empelados/empleados";

@Injectable({
  providedIn: 'root',
})
export class PreBajaService extends PreBajaData {
  private baseURL;
  constructor(private http: HttpClient) {
    super();
    this.baseURL = environment.GLOBAL_SERVICIOS;
  }
  getAllPreBaja(): Observable<PreBaja[]> {
    return this.http.get<PreBaja[]>(this.baseURL + 'v1/pre-baja');
  }
  getAllPreBajaView(): Observable<PreBaja[]> {
    return this.http.get<PreBaja[]>(this.baseURL + 'v1/pre-baja-view');
  }
  getPreBajaById(idEmpleado): Observable<PreBaja> {
    return this.http.get<PreBaja>(this.baseURL + 'v1/pre-baja' + '/' + idEmpleado);
  }
  getfiltradoPreBaja(idSubarea): Observable<Empleados[]> {
    return this.http.get<Empleados[]>(this.baseURL + 'v1/pre-baja/get-empleados-pre-baja/' + idSubarea);
  }
  postPreBaja(empleado: PreBaja): Observable<PreBaja> {
    return this.http.post<PreBaja>( this.baseURL + 'v1/pre-baja', empleado);
  }
  putPreBaja(idEmpleado: number, empleado: PreBaja) {
    return this.http.put<PreBaja>(this.baseURL + 'v1/pre-baja' + '/' + idEmpleado, empleado);
  }
}
