import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ejemplos',
  templateUrl: './ejemplos.component.html',
  styleUrls: ['./ejemplos.component.css']
})
export class EjemplosComponent implements OnInit {

  constructor() {}

  ngOnInit() {}

  sweetAlert() {
    Swal.fire({
      title: 'INFO MODAL',
      text: 'los colores de mis botones pueden ser personalizados',
      showCancelButton: true,
      showConfirmButton: true,
      confirmButtonColor: '#78c337',
      cancelButtonColor: '#c33737',
      confirmButtonText: 'Modal 2',
      cancelButtonText: 'Modal 4',
      icon: 'info',
    }).then(resp => {
      if (resp.value) {
        Swal.fire({
          title: 'SUCCESS MODAL',
          text: 'los colores de los botones pueden ser personalizados',
          showConfirmButton: true,
          confirmButtonColor: '#1d2671',
          confirmButtonText: 'Modal 3',
          icon: 'success',
        }).then(resp2 => {
          if (resp2.value) {
            Swal.fire({
              title: 'ERROR MODAL',
              text: 'los colores de los botones pueden ser personalizados',
              showConfirmButton: true,
              confirmButtonColor: '#c33737',
              confirmButtonText: 'Ok',
              icon: 'error',
            });
          }
        });
      } else {
        Swal.fire({
          title: 'WARNING MODAL',
          text: 'los colores de los botones pueden ser personalizados',
          showConfirmButton: true,
          confirmButtonColor: '#e3b43e',
          confirmButtonText: 'Ok',
          icon: 'warning',
        });
      }
    });
  }
}
