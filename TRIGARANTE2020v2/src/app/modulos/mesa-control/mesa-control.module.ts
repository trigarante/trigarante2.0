import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MesaControlRoutingModule } from './mesa-control-routing.module';
import {MesaControlComponent} from './mesa-control.component';


@NgModule({
  declarations: [
    MesaControlComponent
  ],
  imports: [
    CommonModule,
    MesaControlRoutingModule
  ]
})
export class MesaControlModule { }
