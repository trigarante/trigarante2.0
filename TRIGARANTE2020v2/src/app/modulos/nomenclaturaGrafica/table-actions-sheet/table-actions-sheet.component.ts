import {Component, Input, OnInit} from '@angular/core';
import {MatBottomSheetRef} from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-table-actions-sheet',
  templateUrl: './table-actions-sheet.component.html',
  styleUrls: ['./table-actions-sheet.component.css']
})
export class TableActionsSheetComponent implements OnInit {
  constructor(private bottomSheetRef: MatBottomSheetRef<TableActionsSheetComponent>) {}

  ngOnInit(): void {
  }
  openLink(event: MouseEvent): void {
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }
}
