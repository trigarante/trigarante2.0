import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudInternoComponent } from './solicitud-interno.component';

describe('SolicitudInternoComponent', () => {
  let component: SolicitudInternoComponent;
  let fixture: ComponentFixture<SolicitudInternoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudInternoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudInternoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
