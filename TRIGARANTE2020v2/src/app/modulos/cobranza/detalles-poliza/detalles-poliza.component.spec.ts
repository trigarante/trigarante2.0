import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesPolizaComponent } from './detalles-poliza.component';

describe('DetallesPolizaComponent', () => {
  let component: DetallesPolizaComponent;
  let fixture: ComponentFixture<DetallesPolizaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallesPolizaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesPolizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
