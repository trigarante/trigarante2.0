import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecibosSubsecuentesComponent } from './recibos-subsecuentes.component';

describe('RecibosSubsecuentesComponent', () => {
  let component: RecibosSubsecuentesComponent;
  let fixture: ComponentFixture<RecibosSubsecuentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecibosSubsecuentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecibosSubsecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
