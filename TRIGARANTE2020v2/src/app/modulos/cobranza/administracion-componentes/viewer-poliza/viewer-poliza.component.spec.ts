import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewerPolizaComponent } from './viewer-poliza.component';

describe('ViewerPolizaComponent', () => {
  let component: ViewerPolizaComponent;
  let fixture: ComponentFixture<ViewerPolizaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewerPolizaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewerPolizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
