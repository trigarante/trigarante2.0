import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivacionEmpleadoCobranzaComponent } from './activacion-empleado-cobranza.component';

describe('ActivacionEmpleadoCobranzaComponent', () => {
  let component: ActivacionEmpleadoCobranzaComponent;
  let fixture: ComponentFixture<ActivacionEmpleadoCobranzaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivacionEmpleadoCobranzaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivacionEmpleadoCobranzaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
