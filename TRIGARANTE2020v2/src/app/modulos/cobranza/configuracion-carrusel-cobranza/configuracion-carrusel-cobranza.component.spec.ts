import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfiguracionCarruselCobranzaComponent } from './configuracion-carrusel-cobranza.component';

describe('ConfiguracionCarruselCobranzaComponent', () => {
  let component: ConfiguracionCarruselCobranzaComponent;
  let fixture: ComponentFixture<ConfiguracionCarruselCobranzaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfiguracionCarruselCobranzaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfiguracionCarruselCobranzaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
