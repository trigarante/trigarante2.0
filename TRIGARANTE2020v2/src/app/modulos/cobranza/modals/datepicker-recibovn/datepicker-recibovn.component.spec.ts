import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatepickerRecibovnComponent } from './datepicker-recibovn.component';

describe('DatepickerRecibovnComponent', () => {
  let component: DatepickerRecibovnComponent;
  let fixture: ComponentFixture<DatepickerRecibovnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatepickerRecibovnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatepickerRecibovnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
