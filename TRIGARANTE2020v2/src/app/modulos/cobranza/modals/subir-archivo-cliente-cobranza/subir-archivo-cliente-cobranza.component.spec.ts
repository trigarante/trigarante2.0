import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubirArchivoClienteCobranzaComponent } from './subir-archivo-cliente-cobranza.component';

describe('SubirArchivoClienteCobranzaComponent', () => {
  let component: SubirArchivoClienteCobranzaComponent;
  let fixture: ComponentFixture<SubirArchivoClienteCobranzaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubirArchivoClienteCobranzaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubirArchivoClienteCobranzaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
