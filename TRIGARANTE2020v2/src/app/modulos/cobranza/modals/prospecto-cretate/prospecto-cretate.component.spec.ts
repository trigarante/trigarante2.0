import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProspectoCretateComponent } from './prospecto-cretate.component';

describe('ProspectoCretateComponent', () => {
  let component: ProspectoCretateComponent;
  let fixture: ComponentFixture<ProspectoCretateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProspectoCretateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProspectoCretateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
