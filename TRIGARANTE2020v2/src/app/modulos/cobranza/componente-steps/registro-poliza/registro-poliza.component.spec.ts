import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroPolizaComponent } from './registro-poliza.component';

describe('RegistroPolizaComponent', () => {
  let component: RegistroPolizaComponent;
  let fixture: ComponentFixture<RegistroPolizaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroPolizaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroPolizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
