import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearAutorizacionCobranzaComponent } from './crear-autorizacion-cobranza.component';

describe('CrearAutorizacionCobranzaComponent', () => {
  let component: CrearAutorizacionCobranzaComponent;
  let fixture: ComponentFixture<CrearAutorizacionCobranzaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearAutorizacionCobranzaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearAutorizacionCobranzaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
