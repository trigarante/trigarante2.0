import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorizacionesProcesoCobranzaComponent } from './autorizaciones-proceso-cobranza.component';

describe('AutorizacionesProcesoCobranzaComponent', () => {
  let component: AutorizacionesProcesoCobranzaComponent;
  let fixture: ComponentFixture<AutorizacionesProcesoCobranzaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutorizacionesProcesoCobranzaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorizacionesProcesoCobranzaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
