import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CobranzaRoutingModule } from './cobranza-routing.module';
import { CobranzaComponent } from './cobranza.component';
import { ActivacionEmpleadoCobranzaComponent } from './activacion-empleado-cobranza/activacion-empleado-cobranza.component';
import { AdministracionRecibosComponent } from './administracion-componentes/administracion-recibos/administracion-recibos.component';
import { DatosClienteComponent } from './administracion-componentes/datos-cliente/datos-cliente.component';
import { PagoRecibosComponent } from './administracion-componentes/pago-recibos/pago-recibos.component';
import { RecibosSubsecuentesComponent } from './administracion-componentes/recibos-subsecuentes/recibos-subsecuentes.component';
import { ViewerPolizaComponent } from './administracion-componentes/viewer-poliza/viewer-poliza.component';
import { AdministracionPolizasComponent } from './administracion-polizas/administracion-polizas.component';
import { AutorizacionesProcesoCobranzaComponent } from './autorizacion-errores-cobranza/autorizaciones-proceso-cobranza/autorizaciones-proceso-cobranza.component';
import { CrearAutorizacionCobranzaComponent } from './autorizacion-errores-cobranza/crear-autorizacion-cobranza/crear-autorizacion-cobranza.component';
import { MostrarDocumentosAutorizacionCobranzaComponent } from './autorizacion-errores-cobranza/mostrar-documentos-autorizacion-cobranza/mostrar-documentos-autorizacion-cobranza.component';
import { CobranzaSubSecuentesComponent } from './cobranza-sub-secuentes/cobranza-sub-secuentes.component';
import { CreateEmisionComponent } from './componente-steps/create-emision/create-emision.component';
import { DatosProductoComponent } from './componente-steps/datos-producto/datos-producto.component';
import { InspeccionesComponent } from './componente-steps/inspecciones/inspecciones.component';
import { PagosComponent } from './componente-steps/pagos/pagos.component';
import { RecibosComponent } from './componente-steps/recibos/recibos.component';
import { RegistroPolizaComponent } from './componente-steps/registro-poliza/registro-poliza.component';
import { ConfiguracionCarruselCobranzaComponent } from './configuracion-carrusel-cobranza/configuracion-carrusel-cobranza.component';
import { CorreccionErroresCobranzaComponent } from './correccion-errores-cobranza/correccion-errores-cobranza.component';
import { CotizadorComponent } from './cotizador/cotizador.component';
import { CotizadorInternoComponent } from './cotizador-interno/cotizador-interno.component';
import { CotizadorTabsComponent } from './cotizador-tabs/cotizador-tabs.component';
import { DetallesPolizaComponent } from './detalles-poliza/detalles-poliza.component';
import { DocumentosCorreccionCobranzaComponent } from './documentos-correccion-cobranza/documentos-correccion-cobranza.component';
import { SolicitudInternoComponent } from './solicitud-interno/solicitud-interno.component';
import { StepCotizadorComponent } from './step-cotizador/step-cotizador.component';
import { BajaRecibosComponent } from './modals/baja-recibos/baja-recibos.component';
import { DatepickerRecibovnComponent } from './modals/datepicker-recibovn/datepicker-recibovn.component';
import { DatosSolicitudComponent } from './modals/datos-solicitud/datos-solicitud.component';
import { EndosoCreateComponent } from './modals/endoso-create/endoso-create.component';
import { EtiquetaSolicitudComponent } from './modals/etiqueta-solicitud/etiqueta-solicitud.component';
import { ProspectoCretateComponent } from './modals/prospecto-cretate/prospecto-cretate.component';
import { ReasignarSolicitudComponent } from './modals/reasignar-solicitud/reasignar-solicitud.component';
import { SolicitudEndosoCreateComponent } from './modals/solicitud-endoso-create/solicitud-endoso-create.component';
import { SubirArchivoClienteCobranzaComponent } from './modals/subir-archivo-cliente-cobranza/subir-archivo-cliente-cobranza.component';


@NgModule({
  declarations: [
    CobranzaComponent,
    ActivacionEmpleadoCobranzaComponent,
    AdministracionRecibosComponent,
    DatosClienteComponent,
    PagoRecibosComponent,
    RecibosSubsecuentesComponent,
    ViewerPolizaComponent,
    AdministracionPolizasComponent,
    AutorizacionesProcesoCobranzaComponent,
    CrearAutorizacionCobranzaComponent,
    MostrarDocumentosAutorizacionCobranzaComponent,
    CobranzaSubSecuentesComponent,
    CreateEmisionComponent,
    DatosProductoComponent,
    InspeccionesComponent,
    PagosComponent,
    RecibosComponent,
    RegistroPolizaComponent,
    ConfiguracionCarruselCobranzaComponent,
    CorreccionErroresCobranzaComponent,
    CotizadorComponent,
    CotizadorInternoComponent,
    CotizadorTabsComponent,
    DetallesPolizaComponent,
    DocumentosCorreccionCobranzaComponent,
    SolicitudInternoComponent,
    StepCotizadorComponent,
    BajaRecibosComponent,
    DatepickerRecibovnComponent,
    DatosSolicitudComponent,
    EndosoCreateComponent,
    EtiquetaSolicitudComponent,
    ProspectoCretateComponent,
    ReasignarSolicitudComponent,
    SolicitudEndosoCreateComponent,
    SubirArchivoClienteCobranzaComponent

  ],
  imports: [
    CommonModule,
    CobranzaRoutingModule
  ]
})
export class CobranzaModule { }
