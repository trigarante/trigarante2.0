import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AutorizacionesRoutingModule } from './autorizaciones-routing.module';
import {AutorizacionesComponent} from './autorizaciones.component';


@NgModule({
  declarations: [
    AutorizacionesComponent
  ],
  imports: [
    CommonModule,
    AutorizacionesRoutingModule
  ]
})
export class AutorizacionesModule { }
