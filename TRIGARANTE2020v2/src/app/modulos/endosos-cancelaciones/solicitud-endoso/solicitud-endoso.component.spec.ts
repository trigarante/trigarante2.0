import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudEndosoComponent } from './solicitud-endoso.component';

describe('SolicitudEndosoComponent', () => {
  let component: SolicitudEndosoComponent;
  let fixture: ComponentFixture<SolicitudEndosoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudEndosoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudEndosoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
