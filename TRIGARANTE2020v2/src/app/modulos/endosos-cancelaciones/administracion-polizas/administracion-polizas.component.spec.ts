import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministracionPolizasComponent } from './administracion-polizas.component';

describe('AdministracionPolizasComponent', () => {
  let component: AdministracionPolizasComponent;
  let fixture: ComponentFixture<AdministracionPolizasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministracionPolizasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministracionPolizasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
