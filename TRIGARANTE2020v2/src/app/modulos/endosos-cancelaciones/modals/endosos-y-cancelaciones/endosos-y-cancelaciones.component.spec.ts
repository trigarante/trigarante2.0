import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndososYCancelacionesComponent } from './endosos-y-cancelaciones.component';

describe('EndososYCancelacionesComponent', () => {
  let component: EndososYCancelacionesComponent;
  let fixture: ComponentFixture<EndososYCancelacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndososYCancelacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndososYCancelacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
