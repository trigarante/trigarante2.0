import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleEndosoComponent } from './detalle-endoso.component';

describe('DetalleEndosoComponent', () => {
  let component: DetalleEndosoComponent;
  let fixture: ComponentFixture<DetalleEndosoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleEndosoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleEndosoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
