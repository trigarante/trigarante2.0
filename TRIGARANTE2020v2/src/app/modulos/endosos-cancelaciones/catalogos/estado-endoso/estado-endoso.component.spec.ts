import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoEndosoComponent } from './estado-endoso.component';

describe('EstadoEndosoComponent', () => {
  let component: EstadoEndosoComponent;
  let fixture: ComponentFixture<EstadoEndosoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoEndosoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoEndosoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
