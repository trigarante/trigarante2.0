import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoEndosoCreateComponent } from './tipo-endoso-create.component';

describe('TipoEndosoCreateComponent', () => {
  let component: TipoEndosoCreateComponent;
  let fixture: ComponentFixture<TipoEndosoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoEndosoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoEndosoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
