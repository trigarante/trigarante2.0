import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoEndosoComponent } from './tipo-endoso.component';

describe('TipoEndosoComponent', () => {
  let component: TipoEndosoComponent;
  let fixture: ComponentFixture<TipoEndosoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoEndosoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoEndosoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
