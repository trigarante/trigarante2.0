import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndosoYCancelacionesComponent } from './endoso-y-cancelaciones.component';

describe('EndosoYCancelacionesComponent', () => {
  let component: EndosoYCancelacionesComponent;
  let fixture: ComponentFixture<EndosoYCancelacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndosoYCancelacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndosoYCancelacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
