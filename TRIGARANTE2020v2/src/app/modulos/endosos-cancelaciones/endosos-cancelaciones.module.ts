import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EndososCancelacionesRoutingModule } from './endosos-cancelaciones-routing.module';
import { EndososCancelacionesComponent } from './endosos-cancelaciones.component';
import { DetallesPolizaComponent } from './administracion-componentes/detalles-poliza/detalles-poliza.component';
import { ViewerPolizaComponent } from './administracion-componentes/viewer-poliza/viewer-poliza.component';
import { AdministracionPolizasComponent } from './administracion-polizas/administracion-polizas.component';
import { EstadoEndosoComponent } from './catalogos/estado-endoso/estado-endoso.component';
import { EstadoSolicitudEndosoComponent } from './catalogos/estado-solicitud-endoso/estado-solicitud-endoso.component';
import { TipoEndosoComponent } from './catalogos/tipo-endoso/tipo-endoso.component';
import { EstadoEndosoCreateComponent } from './catalogos/modals/estado-endoso-create/estado-endoso-create.component';
import { EstadoSolicitudEndosoCreateComponent } from './catalogos/modals/estado-solicitud-endoso-create/estado-solicitud-endoso-create.component';
import { TipoEndosoCreateComponent } from './catalogos/modals/tipo-endoso-create/tipo-endoso-create.component';
import { CompletadasComponent } from './completadas/completadas.component';
import { EndosoYCancelacionesComponent } from './endoso-y-cancelaciones/endoso-y-cancelaciones.component';
import { SolicitudEndosoComponent } from './solicitud-endoso/solicitud-endoso.component';
import { DetalleEndosoComponent } from './modals/detalle-endoso/detalle-endoso.component';
import { EndososYCancelacionesComponent } from './modals/endosos-y-cancelaciones/endosos-y-cancelaciones.component';
import { EnviarEndosoComponent } from './modals/enviar-endoso/enviar-endoso.component';
import { FinalizarEndosoComponent } from './modals/finalizar-endoso/finalizar-endoso.component';
import { MandarCorreccionComponent } from './modals/mandar-correccion/mandar-correccion.component';
import { ModificarEstadoEndosoComponent } from './modals/modificar-estado-endoso/modificar-estado-endoso.component';
import { ModificarPrimaComponent } from './modals/modificar-prima/modificar-prima.component';
import { ReasignarSolicitudComponent } from './modals/reasignar-solicitud/reasignar-solicitud.component';
import { SolicitudEndosoCreateComponent } from './modals/solicitud-endoso-create/solicitud-endoso-create.component';


@NgModule({
  declarations: [
    EndososCancelacionesComponent,
    DetallesPolizaComponent,
    ViewerPolizaComponent,
    AdministracionPolizasComponent,
    EstadoEndosoComponent,
    EstadoSolicitudEndosoComponent,
    TipoEndosoComponent,
    EstadoEndosoCreateComponent,
    EstadoSolicitudEndosoCreateComponent,
    TipoEndosoCreateComponent,
    CompletadasComponent,
    EndosoYCancelacionesComponent,
    SolicitudEndosoComponent,
    DetalleEndosoComponent,
    EndososYCancelacionesComponent,
    EnviarEndosoComponent,
    FinalizarEndosoComponent,
    MandarCorreccionComponent,
    ModificarEstadoEndosoComponent,
    ModificarPrimaComponent,
    ReasignarSolicitudComponent,
    SolicitudEndosoCreateComponent

  ],
  imports: [
    CommonModule,
    EndososCancelacionesRoutingModule
  ]
})
export class EndososCancelacionesModule { }
