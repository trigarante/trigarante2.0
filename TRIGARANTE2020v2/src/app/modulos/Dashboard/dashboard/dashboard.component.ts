import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  objects = {id: 1, nombre: 'Sheldon', apellidos: 'Cooper Sanchez', conversion: 23, contactos: 30, polizasEmitidas: 5,
    polizasNoEmitidas: 0, polizasCobradas: 50, polizasNoCobradas: 0, polizasAplicadas: 18, polizasNoAplicadas: 0, correo: 'scooper@trigarantte.com'};
  vendidas = 37 + '%';
  puntualidad = 30 + '%';
  comisiones = 20 + '%';
  errPolizas = 13 + '%';
  saludo = '';
  motivacion = '';
  getUrl = window.location.pathname;
  interaccion = false;
  constructor(
    private router: Router,
  ) {
  }

  ngOnInit() {
    console.log(this.getUrl);
    this.interaccion = this.getUrl === '/dasboard' ? true : false;
    this.dateFunction();
    this.mostrarSaludo();
  }

  dateFunction() {
    function formatTitleDate(date = new Date()) {
      const { day, month, year, weekday } = new Intl.DateTimeFormat('es', {
        day: '2-digit',
        month: 'short',
        year: 'numeric',
        weekday: 'long',
      }).formatToParts(date).reduce((acc, part) => {
        if (part.type !== 'literal') {
          acc[part.type] = part.value;
        }
        return acc;
      }, Object.create(null));
      return `${day} ${month} ${year}, ${weekday}`;
    }

    document.getElementsByClassName('date')[0].textContent = formatTitleDate();
  }

  mostrarSaludo(){
    const fecha = new Date();
    const hora = fecha.getHours();

    if (hora >= 0 && hora < 12){
      this.saludo = 'Buenos Días.';
      this.motivacion = 'Antes de rendirte recuerda la razón por la cual empezaste.';
    }

    if (hora >= 12 && hora < 18){
      this.saludo = 'Buenas Tardes.';
      this.motivacion = 'Conserva tu sueños, nunca sabes cuando te harán falta.';
    }

    if (hora >= 18 && hora < 24){
      this.saludo = 'Buenas Noches.';
      this.motivacion = 'No esperes nada de nadie, espera todo de ti.';
    }
  }

  logOut() {
    Swal.fire({
      title: '¿Desea cerrar sesión?',
      text: '',
      icon: 'question',
      showConfirmButton: true,
      showCancelButton: true,
      cancelButtonColor: '#C33764',
      cancelButtonText: 'No',
      confirmButtonColor: '#1D2671',
      confirmButtonText: 'Si',
      allowOutsideClick: false,
      allowEscapeKey: false,
    }).then(tkn => {
      if (tkn.value) {
        Swal.fire({
          title: 'Sesión cerrada',
          text: '',
          icon: 'success',
          confirmButtonColor: '#1D2671',
          confirmButtonText: 'Ok',
          allowOutsideClick: false,
          allowEscapeKey: false,
        }).then(resp => {
          if (resp.value) {
            this.router.navigate(['login']);
          }
        });
      }
    });
  }
}
