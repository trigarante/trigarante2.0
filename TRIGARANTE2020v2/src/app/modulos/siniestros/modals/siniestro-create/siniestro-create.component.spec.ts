import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiniestroCreateComponent } from './siniestro-create.component';

describe('SiniestroCreateComponent', () => {
  let component: SiniestroCreateComponent;
  let fixture: ComponentFixture<SiniestroCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiniestroCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiniestroCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
