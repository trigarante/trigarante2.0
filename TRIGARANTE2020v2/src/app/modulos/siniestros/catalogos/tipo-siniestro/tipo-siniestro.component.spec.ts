import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoSiniestroComponent } from './tipo-siniestro.component';

describe('TipoSiniestroComponent', () => {
  let component: TipoSiniestroComponent;
  let fixture: ComponentFixture<TipoSiniestroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoSiniestroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoSiniestroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
