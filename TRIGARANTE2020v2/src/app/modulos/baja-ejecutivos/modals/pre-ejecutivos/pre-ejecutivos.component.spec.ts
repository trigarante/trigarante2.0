import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreEjecutivosComponent } from './pre-ejecutivos.component';

describe('PreEjecutivosComponent', () => {
  let component: PreEjecutivosComponent;
  let fixture: ComponentFixture<PreEjecutivosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreEjecutivosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreEjecutivosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
