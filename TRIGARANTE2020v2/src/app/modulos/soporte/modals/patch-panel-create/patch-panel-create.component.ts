import { Component, OnInit } from '@angular/core';
import {PatchPanelService} from '../../../../@core/data/services/soporte/inventario-soporte/patch-panel.service';

@Component({
  selector: 'app-patch-panel-create',
  templateUrl: './patch-panel-create.component.html',
  styleUrls: ['./patch-panel-create.component.scss']
})
export class PatchPanelCreateComponent implements OnInit {

  constructor(private _PatchPanelService: PatchPanelService) { }

  ngOnInit(): void {
  }

}
