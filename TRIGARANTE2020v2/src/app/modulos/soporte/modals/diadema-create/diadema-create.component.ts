import { Component, OnInit } from '@angular/core';
import {DiademaService} from '../../../../@core/data/services/soporte/inventario-soporte/diadema.service';

@Component({
  selector: 'app-diadema-create',
  templateUrl: './diadema-create.component.html',
  styleUrls: ['./diadema-create.component.scss']
})
export class DiademaCreateComponent implements OnInit {

  constructor(private _DiademaService: DiademaService) { }

  ngOnInit(): void {
  }

}
