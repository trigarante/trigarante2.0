import { Component, OnInit } from '@angular/core';
import {RacksService} from '../../../../@core/data/services/soporte/inventario-soporte/racks.service';

@Component({
  selector: 'app-racks-create',
  templateUrl: './racks-create.component.html',
  styleUrls: ['./racks-create.component.scss']
})
export class RacksCreateComponent implements OnInit {

  constructor(private _RacksService: RacksService) { }

  ngOnInit(): void {
  }

}
