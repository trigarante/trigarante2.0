import { Component, OnInit } from '@angular/core';
import {BiometricosService} from '../../../../@core/data/services/soporte/inventario-soporte/biometricos.service';

@Component({
  selector: 'app-biometricos-create',
  templateUrl: './biometricos-create.component.html',
  styleUrls: ['./biometricos-create.component.scss']
})
export class BiometricosCreateComponent implements OnInit {

  constructor(private _BiometricosService: BiometricosService) { }

  ngOnInit(): void {
  }

}
