import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharolasRackCreateComponent } from './charolas-rack-create.component';

describe('CharolasRackCreateComponent', () => {
  let component: CharolasRackCreateComponent;
  let fixture: ComponentFixture<CharolasRackCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharolasRackCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharolasRackCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
