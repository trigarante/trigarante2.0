import { Component, OnInit } from '@angular/core';
import {DiscoDuroInternoService} from '../../../../@core/data/services/soporte/inventario-soporte/disco-duro-interno.service';

@Component({
  selector: 'app-disco-duro-interno-create',
  templateUrl: './disco-duro-interno-create.component.html',
  styleUrls: ['./disco-duro-interno-create.component.scss']
})
export class DiscoDuroInternoCreateComponent implements OnInit {

  constructor(private _DiscoDuroInternoService: DiscoDuroInternoService) { }

  ngOnInit(): void {
  }

}
