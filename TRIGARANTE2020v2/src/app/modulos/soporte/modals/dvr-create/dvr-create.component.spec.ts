import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DvrCreateComponent } from './dvr-create.component';

describe('DvrCreateComponent', () => {
  let component: DvrCreateComponent;
  let fixture: ComponentFixture<DvrCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DvrCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DvrCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
