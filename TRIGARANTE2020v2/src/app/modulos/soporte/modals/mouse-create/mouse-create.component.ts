import { Component, OnInit } from '@angular/core';
import {MouseService} from '../../../../@core/data/services/soporte/inventario-soporte/mouse.service';

@Component({
  selector: 'app-mouse-create',
  templateUrl: './mouse-create.component.html',
  styleUrls: ['./mouse-create.component.scss']
})
export class MouseCreateComponent implements OnInit {

  constructor(private _MouseService: MouseService) { }

  ngOnInit(): void {
  }

}
