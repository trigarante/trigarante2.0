import { Component, OnInit } from '@angular/core';
import {CamarasService} from '../../../../@core/data/services/soporte/inventario-soporte/camaras.service';

@Component({
  selector: 'app-camara-create',
  templateUrl: './camara-create.component.html',
  styleUrls: ['./camara-create.component.scss']
})
export class CamaraCreateComponent implements OnInit {

  constructor(private _CamarasService: CamarasService) { }

  ngOnInit(): void {
  }

}
