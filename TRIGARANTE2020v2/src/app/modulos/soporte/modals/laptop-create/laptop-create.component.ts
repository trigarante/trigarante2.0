import { Component, OnInit } from '@angular/core';
import {LaptopService} from '../../../../@core/data/services/soporte/inventario-soporte/laptop.service';

@Component({
  selector: 'app-laptop-create',
  templateUrl: './laptop-create.component.html',
  styleUrls: ['./laptop-create.component.scss']
})
export class LaptopCreateComponent implements OnInit {

  constructor(private _LaptopService: LaptopService) { }

  ngOnInit(): void {
  }

}
