import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarraMulticontactoCreateComponent } from './barra-multicontacto-create.component';

describe('BarraMulticontactoCreateComponent', () => {
  let component: BarraMulticontactoCreateComponent;
  let fixture: ComponentFixture<BarraMulticontactoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarraMulticontactoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarraMulticontactoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
