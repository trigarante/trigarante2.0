import { Component, OnInit } from '@angular/core';
import {BarrasMulticontactoService} from '../../../../@core/data/services/soporte/inventario-soporte/barras-multicontacto.service';

@Component({
  selector: 'app-barra-multicontacto-create',
  templateUrl: './barra-multicontacto-create.component.html',
  styleUrls: ['./barra-multicontacto-create.component.scss']
})
export class BarraMulticontactoCreateComponent implements OnInit {

  constructor(private _BarrasMulticontactoService: BarrasMulticontactoService) { }

  ngOnInit(): void {
  }

}
