import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscoDuroExternoCreateComponent } from './disco-duro-externo-create.component';

describe('DiscoDuroExternoCreateComponent', () => {
  let component: DiscoDuroExternoCreateComponent;
  let fixture: ComponentFixture<DiscoDuroExternoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscoDuroExternoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscoDuroExternoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
