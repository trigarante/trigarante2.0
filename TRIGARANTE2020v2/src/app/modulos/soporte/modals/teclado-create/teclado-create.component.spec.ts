import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TecladoCreateComponent } from './teclado-create.component';

describe('TecladoCreateComponent', () => {
  let component: TecladoCreateComponent;
  let fixture: ComponentFixture<TecladoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TecladoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TecladoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
