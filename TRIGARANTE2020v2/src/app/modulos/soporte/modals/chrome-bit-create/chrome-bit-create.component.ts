import { Component, OnInit } from '@angular/core';
import {ChromeBitService} from '../../../../@core/data/services/soporte/inventario-soporte/chrome-bit.service';

@Component({
  selector: 'app-chrome-bit-create',
  templateUrl: './chrome-bit-create.component.html',
  styleUrls: ['./chrome-bit-create.component.scss']
})
export class ChromeBitCreateComponent implements OnInit {

  constructor(private _ChromeBitService: ChromeBitService) { }

  ngOnInit(): void {
  }

}
