import { Component, OnInit } from '@angular/core';
import {TelefonoIpService} from '../../../../@core/data/services/soporte/inventario-soporte/telefono-ip.service';

@Component({
  selector: 'app-telefono-ip-create',
  templateUrl: './telefono-ip-create.component.html',
  styleUrls: ['./telefono-ip-create.component.scss']
})
export class TelefonoIpCreateComponent implements OnInit {

  constructor(private _TelefonoIpService: TelefonoIpService) { }

  ngOnInit(): void {
  }

}
