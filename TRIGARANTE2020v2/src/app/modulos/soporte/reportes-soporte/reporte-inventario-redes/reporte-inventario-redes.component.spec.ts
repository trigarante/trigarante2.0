import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteInventarioRedesComponent } from './reporte-inventario-redes.component';

describe('ReporteInventarioRedesComponent', () => {
  let component: ReporteInventarioRedesComponent;
  let fixture: ComponentFixture<ReporteInventarioRedesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteInventarioRedesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteInventarioRedesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
