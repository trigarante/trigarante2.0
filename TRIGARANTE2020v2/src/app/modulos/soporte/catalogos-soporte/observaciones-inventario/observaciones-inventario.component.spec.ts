import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObservacionesInventarioComponent } from './observaciones-inventario.component';

describe('ObservacionesInventarioComponent', () => {
  let component: ObservacionesInventarioComponent;
  let fixture: ComponentFixture<ObservacionesInventarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObservacionesInventarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObservacionesInventarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
