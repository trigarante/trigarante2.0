import { Component, OnInit } from '@angular/core';
import {ObservacionesInventarioService} from '../../../../@core/data/services/soporte/catalogos/observaciones-inventario.service';

@Component({
  selector: 'app-observaciones-inventario',
  templateUrl: './observaciones-inventario.component.html',
  styleUrls: ['./observaciones-inventario.component.scss']
})
export class ObservacionesInventarioComponent implements OnInit {

  constructor(private _ObservacionesInventarioService: ObservacionesInventarioService) { }

  ngOnInit(): void {
  }

}
