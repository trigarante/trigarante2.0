import { Component, OnInit } from '@angular/core';
import {EstadoInventarioService} from '../../../../@core/data/services/soporte/catalogos/estado-inventario.service';

@Component({
  selector: 'app-estado-inventario',
  templateUrl: './estado-inventario.component.html',
  styleUrls: ['./estado-inventario.component.scss']
})
export class EstadoInventarioComponent implements OnInit {

  constructor(private _EstadoInventarioService: EstadoInventarioService) { }

  ngOnInit(): void {
  }

}
