import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoInventarioComponent } from './estado-inventario.component';

describe('EstadoInventarioComponent', () => {
  let component: EstadoInventarioComponent;
  let fixture: ComponentFixture<EstadoInventarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoInventarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoInventarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
