import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogosSoporteComponent } from './catalogos-soporte.component';

describe('CatalogosSoporteComponent', () => {
  let component: CatalogosSoporteComponent;
  let fixture: ComponentFixture<CatalogosSoporteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogosSoporteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogosSoporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
