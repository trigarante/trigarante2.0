import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BajaInventarioSoporteComponent } from './baja-inventario-soporte.component';

describe('BajaInventarioSoporteComponent', () => {
  let component: BajaInventarioSoporteComponent;
  let fixture: ComponentFixture<BajaInventarioSoporteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BajaInventarioSoporteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BajaInventarioSoporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
