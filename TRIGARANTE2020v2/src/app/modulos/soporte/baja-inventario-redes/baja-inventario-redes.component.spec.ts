import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BajaInventarioRedesComponent } from './baja-inventario-redes.component';

describe('BajaInventarioRedesComponent', () => {
  let component: BajaInventarioRedesComponent;
  let fixture: ComponentFixture<BajaInventarioRedesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BajaInventarioRedesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BajaInventarioRedesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
