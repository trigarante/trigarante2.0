import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PresupuesoCampanaComponent } from './presupueso-campana.component';

describe('PresupuesoCampanaComponent', () => {
  let component: PresupuesoCampanaComponent;
  let fixture: ComponentFixture<PresupuesoCampanaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresupuesoCampanaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresupuesoCampanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
