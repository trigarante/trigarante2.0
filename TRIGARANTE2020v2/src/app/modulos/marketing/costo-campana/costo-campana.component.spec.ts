import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostoCampanaComponent } from './costo-campana.component';

describe('CostoCampanaComponent', () => {
  let component: CostoCampanaComponent;
  let fixture: ComponentFixture<CostoCampanaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostoCampanaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostoCampanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
