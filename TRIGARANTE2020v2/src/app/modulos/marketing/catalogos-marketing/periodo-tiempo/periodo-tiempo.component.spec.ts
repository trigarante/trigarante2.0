import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeriodoTiempoComponent } from './periodo-tiempo.component';

describe('PeriodoTiempoComponent', () => {
  let component: PeriodoTiempoComponent;
  let fixture: ComponentFixture<PeriodoTiempoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeriodoTiempoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeriodoTiempoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
