import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEstadoMetaComponent } from './create-estado-meta.component';

describe('CreateEstadoMetaComponent', () => {
  let component: CreateEstadoMetaComponent;
  let fixture: ComponentFixture<CreateEstadoMetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEstadoMetaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEstadoMetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
