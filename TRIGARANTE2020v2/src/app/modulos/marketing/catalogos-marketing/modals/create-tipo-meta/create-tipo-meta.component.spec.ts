import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTipoMetaComponent } from './create-tipo-meta.component';

describe('CreateTipoMetaComponent', () => {
  let component: CreateTipoMetaComponent;
  let fixture: ComponentFixture<CreateTipoMetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTipoMetaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTipoMetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
