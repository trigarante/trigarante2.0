import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProveedoresLeadCreateComponent } from './proveedores-lead-create.component';

describe('ProveedoresLeadCreateComponent', () => {
  let component: ProveedoresLeadCreateComponent;
  let fixture: ComponentFixture<ProveedoresLeadCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProveedoresLeadCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProveedoresLeadCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
