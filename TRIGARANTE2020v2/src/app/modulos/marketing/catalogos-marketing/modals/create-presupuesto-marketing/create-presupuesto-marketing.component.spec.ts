import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePresupuestoMarketingComponent } from './create-presupuesto-marketing.component';

describe('CreatePresupuestoMarketingComponent', () => {
  let component: CreatePresupuestoMarketingComponent;
  let fixture: ComponentFixture<CreatePresupuestoMarketingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePresupuestoMarketingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePresupuestoMarketingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
