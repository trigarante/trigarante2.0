import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTipoCostoComponent } from './create-tipo-costo.component';

describe('CreateTipoCostoComponent', () => {
  let component: CreateTipoCostoComponent;
  let fixture: ComponentFixture<CreateTipoCostoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTipoCostoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTipoCostoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
