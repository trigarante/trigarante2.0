import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTipoPresupuestoComponent } from './create-tipo-presupuesto.component';

describe('CreateTipoPresupuestoComponent', () => {
  let component: CreateTipoPresupuestoComponent;
  let fixture: ComponentFixture<CreateTipoPresupuestoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTipoPresupuestoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTipoPresupuestoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
