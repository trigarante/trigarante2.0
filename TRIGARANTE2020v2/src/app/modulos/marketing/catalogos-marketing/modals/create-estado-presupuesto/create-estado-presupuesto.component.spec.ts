import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEstadoPresupuestoComponent } from './create-estado-presupuesto.component';

describe('CreateEstadoPresupuestoComponent', () => {
  let component: CreateEstadoPresupuestoComponent;
  let fixture: ComponentFixture<CreateEstadoPresupuestoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEstadoPresupuestoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEstadoPresupuestoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
