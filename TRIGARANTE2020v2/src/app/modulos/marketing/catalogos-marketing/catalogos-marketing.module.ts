import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogosMarketingRoutingModule } from './catalogos-marketing-routing.module';
import { EstadoCampanaComponent } from './estado-campana/estado-campana.component';
import { CatalogosMarketingComponent } from './catalogos-marketing.component';
import { EstadoCostoComponent } from './estado-costo/estado-costo.component';
import { EstadoMetaComponent } from './estado-meta/estado-meta.component';
import { EstadoPresupuestoComponent } from './estado-presupuesto/estado-presupuesto.component';
import { MarcaComercialComponent } from './marca-comercial/marca-comercial.component';
import { PeriodoTiempoComponent } from './periodo-tiempo/periodo-tiempo.component';
import { PresupuestoMarketingComponent } from './presupuesto-marketing/presupuesto-marketing.component';
import { ProveedorLeadComponent } from './proveedor-lead/proveedor-lead.component';
import { TipoCostoComponent } from './tipo-costo/tipo-costo.component';
import { TipoMetaComponent } from './tipo-meta/tipo-meta.component';
import { TipoPaginaComponent } from './tipo-pagina/tipo-pagina.component';
import { TipoPresupuestoComponent } from './tipo-presupuesto/tipo-presupuesto.component';
import { CreateEstadoCampanaComponent } from './modals/create-estado-campana/create-estado-campana.component';
import { CreateEstadoCostoComponent } from './modals/create-estado-costo/create-estado-costo.component';
import { CreateEstadoMetaComponent } from './modals/create-estado-meta/create-estado-meta.component';
import { CreateEstadoPresupuestoComponent } from './modals/create-estado-presupuesto/create-estado-presupuesto.component';
import { CreateMarcaComercialComponent } from './modals/create-marca-comercial/create-marca-comercial.component';
import { CreatePeriodoTiempoComponent } from './modals/create-periodo-tiempo/create-periodo-tiempo.component';
import { CreatePresupuestoMarketingComponent } from './modals/create-presupuesto-marketing/create-presupuesto-marketing.component';
import { CreateTipoCostoComponent } from './modals/create-tipo-costo/create-tipo-costo.component';
import { CreateTipoMetaComponent } from './modals/create-tipo-meta/create-tipo-meta.component';
import { CreateTipoPresupuestoComponent } from './modals/create-tipo-presupuesto/create-tipo-presupuesto.component';
import { ProveedoresLeadCreateComponent } from './modals/proveedores-lead-create/proveedores-lead-create.component';
import { TipoPaginaCreateComponent } from './modals/tipo-pagina-create/tipo-pagina-create.component';


@NgModule({
  declarations: [
    EstadoCampanaComponent,
    CatalogosMarketingComponent,
    EstadoCostoComponent,
    EstadoMetaComponent,
    EstadoPresupuestoComponent,
    MarcaComercialComponent,
    PeriodoTiempoComponent,
    PresupuestoMarketingComponent,
    ProveedorLeadComponent,
    TipoCostoComponent,
    TipoMetaComponent,
    TipoPaginaComponent,
    TipoPresupuestoComponent,
    CreateEstadoCampanaComponent,
    CreateEstadoCostoComponent,
    CreateEstadoMetaComponent,
    CreateEstadoPresupuestoComponent,
    CreateMarcaComercialComponent,
    CreatePeriodoTiempoComponent,
    CreatePresupuestoMarketingComponent,
    CreateTipoCostoComponent,
    CreateTipoMetaComponent,
    CreateTipoPresupuestoComponent,
    ProveedoresLeadCreateComponent,
    TipoPaginaCreateComponent
  ],
  imports: [
    CommonModule,
    CatalogosMarketingRoutingModule
  ]
})
export class CatalogosMarketingModule { }
