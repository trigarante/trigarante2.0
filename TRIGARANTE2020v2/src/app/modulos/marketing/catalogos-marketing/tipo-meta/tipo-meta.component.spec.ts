import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoMetaComponent } from './tipo-meta.component';

describe('TipoMetaComponent', () => {
  let component: TipoMetaComponent;
  let fixture: ComponentFixture<TipoMetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoMetaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoMetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
