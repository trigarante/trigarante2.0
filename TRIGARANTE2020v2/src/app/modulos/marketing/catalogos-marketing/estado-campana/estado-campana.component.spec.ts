import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoCampanaComponent } from './estado-campana.component';

describe('EstadoCampanaComponent', () => {
  let component: EstadoCampanaComponent;
  let fixture: ComponentFixture<EstadoCampanaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoCampanaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoCampanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
