import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoPaginaComponent } from './tipo-pagina.component';

describe('TipoPaginaComponent', () => {
  let component: TipoPaginaComponent;
  let fixture: ComponentFixture<TipoPaginaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoPaginaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoPaginaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
