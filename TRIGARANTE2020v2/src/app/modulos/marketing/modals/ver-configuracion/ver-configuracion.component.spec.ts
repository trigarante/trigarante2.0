import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerConfiguracionComponent } from './ver-configuracion.component';

describe('VerConfiguracionComponent', () => {
  let component: VerConfiguracionComponent;
  let fixture: ComponentFixture<VerConfiguracionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerConfiguracionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerConfiguracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
