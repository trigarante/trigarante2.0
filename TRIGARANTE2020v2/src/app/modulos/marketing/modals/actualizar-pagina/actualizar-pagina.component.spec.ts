import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarPaginaComponent } from './actualizar-pagina.component';

describe('ActualizarPaginaComponent', () => {
  let component: ActualizarPaginaComponent;
  let fixture: ComponentFixture<ActualizarPaginaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarPaginaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarPaginaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
