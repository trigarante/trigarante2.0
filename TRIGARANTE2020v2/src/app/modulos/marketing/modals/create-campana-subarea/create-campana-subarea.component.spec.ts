import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCampanaSubareaComponent } from './create-campana-subarea.component';

describe('CreateCampanaSubareaComponent', () => {
  let component: CreateCampanaSubareaComponent;
  let fixture: ComponentFixture<CreateCampanaSubareaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCampanaSubareaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCampanaSubareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
