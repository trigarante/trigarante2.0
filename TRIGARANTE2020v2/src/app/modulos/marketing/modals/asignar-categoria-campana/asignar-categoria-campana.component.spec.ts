import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarCategoriaCampanaComponent } from './asignar-categoria-campana.component';

describe('AsignarCategoriaCampanaComponent', () => {
  let component: AsignarCategoriaCampanaComponent;
  let fixture: ComponentFixture<AsignarCategoriaCampanaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarCategoriaCampanaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarCategoriaCampanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
