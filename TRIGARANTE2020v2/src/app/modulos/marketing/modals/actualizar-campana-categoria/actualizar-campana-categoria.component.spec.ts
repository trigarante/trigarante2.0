import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarCampanaCategoriaComponent } from './actualizar-campana-categoria.component';

describe('ActualizarCampanaCategoriaComponent', () => {
  let component: ActualizarCampanaCategoriaComponent;
  let fixture: ComponentFixture<ActualizarCampanaCategoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarCampanaCategoriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarCampanaCategoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
