import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMetasCampanaComponent } from './create-metas-campana.component';

describe('CreateMetasCampanaComponent', () => {
  let component: CreateMetasCampanaComponent;
  let fixture: ComponentFixture<CreateMetasCampanaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateMetasCampanaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMetasCampanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
