import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampanaMarketingCreateComponent } from './campana-marketing-create.component';

describe('CampanaMarketingCreateComponent', () => {
  let component: CampanaMarketingCreateComponent;
  let fixture: ComponentFixture<CampanaMarketingCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampanaMarketingCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampanaMarketingCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
