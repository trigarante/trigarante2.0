import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarAseguradoraComponent } from './agregar-aseguradora.component';

describe('AgregarAseguradoraComponent', () => {
  let component: AgregarAseguradoraComponent;
  let fixture: ComponentFixture<AgregarAseguradoraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarAseguradoraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarAseguradoraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
