import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCostoCampanaComponent } from './create-costo-campana.component';

describe('CreateCostoCampanaComponent', () => {
  let component: CreateCostoCampanaComponent;
  let fixture: ComponentFixture<CreateCostoCampanaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCostoCampanaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCostoCampanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
