import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosNodosComponent } from './datos-nodos.component';

describe('DatosNodosComponent', () => {
  let component: DatosNodosComponent;
  let fixture: ComponentFixture<DatosNodosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosNodosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosNodosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
