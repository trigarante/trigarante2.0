import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarNodoComponent } from './agregar-nodo.component';

describe('AgregarNodoComponent', () => {
  let component: AgregarNodoComponent;
  let fixture: ComponentFixture<AgregarNodoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarNodoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarNodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
