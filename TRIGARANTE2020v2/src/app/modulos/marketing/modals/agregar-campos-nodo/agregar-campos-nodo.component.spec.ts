import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarCamposNodoComponent } from './agregar-campos-nodo.component';

describe('AgregarCamposNodoComponent', () => {
  let component: AgregarCamposNodoComponent;
  let fixture: ComponentFixture<AgregarCamposNodoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarCamposNodoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarCamposNodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
