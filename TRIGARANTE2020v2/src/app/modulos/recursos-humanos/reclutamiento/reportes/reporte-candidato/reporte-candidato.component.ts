import { Component, OnInit } from '@angular/core';
import {CandidatosService} from '../../../../../@core/data/services/rrhh/candidatos/candidatos.service';

@Component({
  selector: 'app-reporte-candidato',
  templateUrl: './reporte-candidato.component.html',
  styleUrls: ['./reporte-candidato.component.scss']
})
export class ReporteCandidatoComponent implements OnInit {

  // tslint:disable-next-line:variable-name
  constructor(private _CandidatosService: CandidatosService) { }

  ngOnInit(): void {
  }

}
