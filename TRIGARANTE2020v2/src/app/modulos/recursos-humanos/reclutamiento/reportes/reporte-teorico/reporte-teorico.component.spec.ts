import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteTeoricoComponent } from './reporte-teorico.component';

describe('ReporteTeoricoComponent', () => {
  let component: ReporteTeoricoComponent;
  let fixture: ComponentFixture<ReporteTeoricoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteTeoricoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteTeoricoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
