import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteCapacitacionComponent } from './reporte-capacitacion.component';

describe('ReporteCapacitacionComponent', () => {
  let component: ReporteCapacitacionComponent;
  let fixture: ComponentFixture<ReporteCapacitacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteCapacitacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteCapacitacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
