import { Component, OnInit } from '@angular/core';
import {CapacitacionService} from '../../../../../@core/data/services/rrhh/capacitacion/capacitacion.service';

@Component({
  selector: 'app-reporte-capacitacion',
  templateUrl: './reporte-capacitacion.component.html',
  styleUrls: ['./reporte-capacitacion.component.scss']
})
export class ReporteCapacitacionComponent implements OnInit {

  constructor(private _CapacitacionService: CapacitacionService) { }

  ngOnInit(): void {
  }

}
