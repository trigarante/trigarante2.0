import { Component, OnInit } from '@angular/core';
import {SeguimientoService} from '../../../../@core/data/services/rrhh/seguimiento/seguimiento.service';

@Component({
  selector: 'app-seguimiento',
  templateUrl: './seguimiento.component.html',
  styleUrls: ['./seguimiento.component.scss']
})
export class SeguimientoComponent implements OnInit {

  constructor(private _SeguimientoService: SeguimientoService) { }

  ngOnInit(): void {
  }

}
