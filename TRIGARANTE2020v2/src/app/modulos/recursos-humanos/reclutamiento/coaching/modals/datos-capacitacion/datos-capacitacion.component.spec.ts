import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosCapacitacionComponent } from './datos-capacitacion.component';

describe('DatosCapacitacionComponent', () => {
  let component: DatosCapacitacionComponent;
  let fixture: ComponentFixture<DatosCapacitacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosCapacitacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosCapacitacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
