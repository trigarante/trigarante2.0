import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BajaPrecandidatoComponent } from './baja-precandidato.component';

describe('BajaPrecandidatoComponent', () => {
  let component: BajaPrecandidatoComponent;
  let fixture: ComponentFixture<BajaPrecandidatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BajaPrecandidatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BajaPrecandidatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
