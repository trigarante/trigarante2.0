import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrecandidatosComponent } from './precandidatos.component';

describe('PrecandidatosComponent', () => {
  let component: PrecandidatosComponent;
  let fixture: ComponentFixture<PrecandidatosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrecandidatosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrecandidatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
