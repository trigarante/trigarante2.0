import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeguimientoPracticoComponent } from './seguimiento-practico.component';

describe('SeguimientoPracticoComponent', () => {
  let component: SeguimientoPracticoComponent;
  let fixture: ComponentFixture<SeguimientoPracticoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeguimientoPracticoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeguimientoPracticoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
