import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosCandidatoComponent } from './datos-candidato.component';

describe('DatosCandidatoComponent', () => {
  let component: DatosCandidatoComponent;
  let fixture: ComponentFixture<DatosCandidatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosCandidatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosCandidatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
