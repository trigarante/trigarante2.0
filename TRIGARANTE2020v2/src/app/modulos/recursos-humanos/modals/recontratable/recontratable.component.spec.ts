import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecontratableComponent } from './recontratable.component';

describe('RecontratableComponent', () => {
  let component: RecontratableComponent;
  let fixture: ComponentFixture<RecontratableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecontratableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecontratableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
