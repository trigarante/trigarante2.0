import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosSeguimientoTeoricoComponent } from './datos-seguimiento-teorico.component';

describe('DatosSeguimientoTeoricoComponent', () => {
  let component: DatosSeguimientoTeoricoComponent;
  let fixture: ComponentFixture<DatosSeguimientoTeoricoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosSeguimientoTeoricoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosSeguimientoTeoricoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
