import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosVacantesComponent } from './datos-vacantes.component';

describe('DatosVacantesComponent', () => {
  let component: DatosVacantesComponent;
  let fixture: ComponentFixture<DatosVacantesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosVacantesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosVacantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
