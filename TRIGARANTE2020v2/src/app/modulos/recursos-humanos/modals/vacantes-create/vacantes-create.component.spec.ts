import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VacantesCreateComponent } from './vacantes-create.component';

describe('VacantesCreateComponent', () => {
  let component: VacantesCreateComponent;
  let fixture: ComponentFixture<VacantesCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VacantesCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VacantesCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
