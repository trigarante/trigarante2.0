import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecursosHumanosRoutingModule } from './recursos-humanos-routing.module';


// Servicios
import {PrecandidatosService} from '../../@core/data/services/rrhh/precandidatos/precandidatos.service';
import {SolicitudesService} from '../../@core/data/services/rrhh/solicitudes/solicitudes.service';
import {CandidatosService} from '../../@core/data/services/rrhh/candidatos/candidatos.service';
import {CapacitacionService} from '../../@core/data/services/rrhh/capacitacion/capacitacion.service';
import {SeguimientoService} from '../../@core/data/services/rrhh/seguimiento/seguimiento.service';
import {PreEmpleadosService} from '../../@core/data/services/rrhh/pre-empleados/pre-empleados.service';
import {BancosService} from '../../@core/data/services/rrhh/catalogos-RH/bancos/bancos.service';
import {BolsaTrabajoService} from '../../@core/data/services/rrhh/catalogos-RH/bolsa-trabajo/bolsa-trabajo.service';
import {CompetenciasService} from '../../@core/data/services/rrhh/catalogos-RH/competencias/competencias.service';
import {CompetenciasCandidatoService} from '../../@core/data/services/rrhh/catalogos-RH/competencias-candidato/competencias-candidato.service';
import {EscolaridadService} from '../../@core/data/services/rrhh/catalogos-RH/escolaridad/escolaridad.service';
import {EstadoCivilService} from '../../@core/data/services/rrhh/catalogos-RH/estado-civil/estado-civil.service';
import {EstadoEscolaridadService} from '../../@core/data/services/rrhh/catalogos-RH/estado-escolaridad/estado-escolaridad.service';
import {MedioTransporteService} from '../../@core/data/services/rrhh/catalogos-RH/medio-transporte/medio-transporte.service';
import {MotivosBajaService} from '../../@core/data/services/rrhh/catalogos-RH/motivos-baja/motivos-baja.service';
import {PaisesService} from '../../@core/data/services/rrhh/catalogos-RH/paises/paises.service';
import {PuestoService} from '../../@core/data/services/rrhh/catalogos-RH/puesto/puesto.service';
import {TipoPuestoService} from '../../@core/data/services/rrhh/catalogos-RH/tipo-puesto/tipo-puesto.service';
import {TurnoService} from '../../@core/data/services/rrhh/catalogos-RH/turno/turno.service';
import {EmpleadosService} from '../../@core/data/services/rrhh/empleados/empleados.service';

// Componentes
import { RecursosHumanosComponent } from './recursos-humanos.component';
import {SolicitudesComponent} from './reclutamiento/solicitudes/solicitudes.component';
import {PrecandidatosComponent} from './reclutamiento/precandidatos/precandidatos.component';
import {VacantesComponent} from './reclutamiento/vacantes/vacantes.component';
import {BajasRHComponent} from './reclutamiento/bajas-rh/bajas-rh.component';
import {CandidatosComponent} from './reclutamiento/candidatos/candidatos.component';
import {CoachingComponent} from './reclutamiento/coaching/coaching.component';
import {SeguimientoComponent} from './reclutamiento/seguimiento/seguimiento.component';
import {PreEmpleadosComponent} from './capital-humano/pre-empleados/pre-empleados.component';
import {CatalogosComponent} from './catalogos/catalogos.component';
import {BancoComponent} from './catalogos/banco/banco.component';
import {BolsaTrabajoComponent} from './catalogos/bolsa-trabajo/bolsa-trabajo.component';
import {CompetenciasComponent} from './catalogos/competencias/competencias.component';
import {CompetenciasCandidatoComponent} from './catalogos/competencias-candidato/competencias-candidato.component';
import {EscolaridadComponent} from './catalogos/escolaridad/escolaridad.component';
import {EstadoEscolaridadComponent} from './catalogos/estado-escolaridad/estado-escolaridad.component';
import {EstadoCivilComponent} from './catalogos/estado-civil/estado-civil.component';
import {MedioTransporteComponent} from './catalogos/medio-transporte/medio-transporte.component';
import {MotivosBajaComponent} from './catalogos/motivos-baja/motivos-baja.component';
import {PaisesComponent} from './catalogos/paises/paises.component';
import {PuestoComponent} from './catalogos/puesto/puesto.component';
import {TipoPuestoComponent} from './catalogos/tipo-puesto/tipo-puesto.component';
import {TurnoComponent} from './catalogos/turno/turno.component';
import {ReporteCapacitacionComponent} from './reclutamiento/reportes/reporte-capacitacion/reporte-capacitacion.component';
import {ReporteCandidatoComponent} from './reclutamiento/reportes/reporte-candidato/reporte-candidato.component';
import {ReportePrecandidatoComponent} from './reclutamiento/reportes/reporte-precandidato/reporte-precandidato.component';
import {EmpleadosComponent} from './capital-humano/empleados/empleados/empleados.component';
import {EmpleadosImssComponent} from './capital-humano/empleados/alta-de-imss/empleados-imss.component';
import {EmpleadoBajaComponent} from './capital-humano/empleados/empleado-baja/empleado-baja.component';
import { AgendarCitaComponent } from './modals/agendar-cita/agendar-cita.component';
import { AsignarCandidatoComponent } from './modals/asignar-candidato/asignar-candidato.component';
import { AsignarCapacitacionComponent } from './modals/asignar-capacitacion/asignar-capacitacion.component';
import { AsignarEtapaPracticaComponent } from './modals/asignar-etapa-practica/asignar-etapa-practica.component';
import { AsignarFiniquitoComponent } from './modals/asignar-finiquito/asignar-finiquito.component';
import { AsignarImssComponent } from './modals/asignar-imss/asignar-imss.component';
import { AsignarSeguimientoComponent } from './modals/asignar-seguimiento/asignar-seguimiento.component';
import { AsistenciaComponent } from './modals/asistencia/asistencia.component';
import { BajaEmpleadoComponent } from './modals/baja-empleado/baja-empleado.component';
import { BajaPrecandidatoComponent } from './modals/baja-precandidato/baja-precandidato.component';
import { CheckDocumentosComponent } from './modals/check-documentos/check-documentos.component';
import { DatosBajaRhComponent } from './modals/datos-baja-rh/datos-baja-rh.component';
import { DatosCandidatoComponent } from './modals/datos-candidato/datos-candidato.component';
import { DatosCapacitacionComponent } from './modals/datos-capacitacion/datos-capacitacion.component';
import { DatosEmpleadoComponent } from './modals/datos-empleado/datos-empleado.component';
import { DatosPrecandidatoComponent } from './modals/datos-precandidato/datos-precandidato.component';
import { DatosPreempleadoComponent } from './modals/datos-preempleado/datos-preempleado.component';
import { DatosSeguimientoPracticoComponent } from './modals/datos-seguimiento-practico/datos-seguimiento-practico.component';
import { DatosSeguimientoPreempleadoComponent } from './modals/datos-seguimiento-preempleado/datos-seguimiento-preempleado.component';
import { DatosSeguimientoTeoricoComponent } from './modals/datos-seguimiento-teorico/datos-seguimiento-teorico.component';
import { DatosSolicitudesComponent } from './modals/datos-solicitudes/datos-solicitudes.component';
import { DatosVacantesComponent } from './modals/datos-vacantes/datos-vacantes.component';
import { FotoCreateComponent } from './modals/foto-create/foto-create.component';
import { GenerarUrlComponent } from './modals/generar-url/generar-url.component';
import { ReactivarEmpleadoComponent } from './modals/reactivar-empleado/reactivar-empleado.component';
import { RecontratableComponent } from './modals/recontratable/recontratable.component';
import { SeguimientoAsistenciaComponent } from './modals/seguimiento-asistencia/seguimiento-asistencia.component';
import { SolicitudBajaComponent } from './modals/solicitud-baja/solicitud-baja.component';
import { SolicitudCreateComponent } from './modals/solicitud-create/solicitud-create.component';
import { VacantesCreateComponent } from './modals/vacantes-create/vacantes-create.component';
import { ReporteEmpleadoComponent } from './capital-humano/reportes/reporte-empleado/reporte-empleado.component';
import { ReporteTeoricoComponent } from './reclutamiento/reportes/reporte-teorico/reporte-teorico.component';
import { VerEmpleadoComponent } from './capital-humano/empleados/ver-empleado/ver-empleado.component';
import { SeguimientoPracticoComponent } from './reclutamiento/seguimiento-practico/seguimiento-practico.component';


@NgModule({
  declarations: [
    RecursosHumanosComponent,
    SolicitudesComponent,
    PrecandidatosComponent,
    VacantesComponent,
    BajasRHComponent,
    CandidatosComponent,
    CoachingComponent,
    SeguimientoComponent,
    PreEmpleadosComponent,
    CatalogosComponent,
    BancoComponent,
    BolsaTrabajoComponent,
    CompetenciasComponent,
    CompetenciasCandidatoComponent,
    EscolaridadComponent,
    EstadoEscolaridadComponent,
    EstadoCivilComponent,
    MedioTransporteComponent,
    MotivosBajaComponent,
    PaisesComponent,
    PuestoComponent,
    TipoPuestoComponent,
    TurnoComponent,
    ReporteCapacitacionComponent,
    ReporteCandidatoComponent,
    ReportePrecandidatoComponent,
    EmpleadosComponent,
    EmpleadosImssComponent,
    EmpleadoBajaComponent,
    RecursosHumanosComponent,
    AgendarCitaComponent,
    AsignarCandidatoComponent,
    AsignarCapacitacionComponent,
    AsignarEtapaPracticaComponent,
    AsignarFiniquitoComponent,
    AsignarImssComponent,
    AsignarSeguimientoComponent,
    AsistenciaComponent,
    BajaEmpleadoComponent,
    BajaPrecandidatoComponent,
    CheckDocumentosComponent,
    DatosBajaRhComponent,
    DatosCandidatoComponent,
    DatosCapacitacionComponent,
    DatosEmpleadoComponent,
    DatosPrecandidatoComponent,
    DatosPreempleadoComponent,
    DatosSeguimientoPracticoComponent,
    DatosSeguimientoPreempleadoComponent,
    DatosSeguimientoTeoricoComponent,
    DatosSolicitudesComponent,
    DatosVacantesComponent,
    FotoCreateComponent,
    GenerarUrlComponent,
    ReactivarEmpleadoComponent,
    RecontratableComponent,
    SeguimientoAsistenciaComponent,
    SolicitudBajaComponent,
    SolicitudCreateComponent,
    VacantesCreateComponent,
    ReporteEmpleadoComponent,
    ReporteTeoricoComponent,
    VerEmpleadoComponent,
    SeguimientoPracticoComponent

  ],
  imports: [
    CommonModule,
    RecursosHumanosRoutingModule
  ],

  providers: [
    PrecandidatosService,
    SolicitudesService,
    CandidatosService,
    CapacitacionService,
    SeguimientoService,
    PreEmpleadosService,
    BancosService,
    BolsaTrabajoService,
    CompetenciasService,
    CompetenciasCandidatoService,
    EscolaridadService,
    EstadoCivilService,
    EstadoEscolaridadService,
    MedioTransporteService,
    MotivosBajaService,
    PaisesService,
    PuestoService,
    TipoPuestoService,
    TurnoService,
    EmpleadosService
  ],
})
export class RecursosHumanosModule { }
