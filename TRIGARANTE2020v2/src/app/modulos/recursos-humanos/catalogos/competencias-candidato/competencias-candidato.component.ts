import { Component, OnInit } from '@angular/core';
import {CompetenciasCandidatoService} from '../../../../@core/data/services/rrhh/catalogos-RH/competencias-candidato/competencias-candidato.service';

@Component({
  selector: 'app-competencias-candidato',
  templateUrl: './competencias-candidato.component.html',
  styleUrls: ['./competencias-candidato.component.scss']
})
export class CompetenciasCandidatoComponent implements OnInit {

  constructor(private _CompetenciasCandidatoService: CompetenciasCandidatoService) { }

  ngOnInit(): void {
  }

}
