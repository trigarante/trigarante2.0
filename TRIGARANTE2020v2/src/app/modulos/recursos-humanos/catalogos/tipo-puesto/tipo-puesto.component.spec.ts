import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoPuestoComponent } from './tipo-puesto.component';

describe('TipoPuestoComponent', () => {
  let component: TipoPuestoComponent;
  let fixture: ComponentFixture<TipoPuestoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoPuestoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoPuestoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
