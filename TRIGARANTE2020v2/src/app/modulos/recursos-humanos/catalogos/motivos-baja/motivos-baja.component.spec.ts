import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MotivosBajaComponent } from './motivos-baja.component';

describe('MotivosBajaComponent', () => {
  let component: MotivosBajaComponent;
  let fixture: ComponentFixture<MotivosBajaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MotivosBajaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MotivosBajaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
