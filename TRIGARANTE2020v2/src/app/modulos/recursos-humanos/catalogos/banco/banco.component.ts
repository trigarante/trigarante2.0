import { Component, OnInit } from '@angular/core';
import {BancosService} from '../../../../@core/data/services/rrhh/catalogos-RH/bancos/bancos.service';

@Component({
  selector: 'app-banco',
  templateUrl: './banco.component.html',
  styleUrls: ['./banco.component.scss']
})
export class BancoComponent implements OnInit {

  constructor(private _BancosService: BancosService) { }

  ngOnInit(): void {
  }

}
