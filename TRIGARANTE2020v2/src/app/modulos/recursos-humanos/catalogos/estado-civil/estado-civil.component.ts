import { Component, OnInit } from '@angular/core';
import {EstadoCivilService} from '../../../../@core/data/services/rrhh/catalogos-RH/estado-civil/estado-civil.service';

@Component({
  selector: 'app-estado-civil',
  templateUrl: './estado-civil.component.html',
  styleUrls: ['./estado-civil.component.scss']
})
export class EstadoCivilComponent implements OnInit {

  constructor(private _EstadoCivilService: EstadoCivilService) { }

  ngOnInit(): void {
  }

}
