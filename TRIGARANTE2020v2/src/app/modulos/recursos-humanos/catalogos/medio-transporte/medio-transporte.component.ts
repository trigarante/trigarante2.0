import { Component, OnInit } from '@angular/core';
import {MedioTransporteService} from '../../../../@core/data/services/rrhh/catalogos-RH/medio-transporte/medio-transporte.service';

@Component({
  selector: 'app-medio-transporte',
  templateUrl: './medio-transporte.component.html',
  styleUrls: ['./medio-transporte.component.scss']
})
export class MedioTransporteComponent implements OnInit {

  constructor(private _MedioTransporteService: MedioTransporteService
  ) { }

  ngOnInit(): void {
  }

}
