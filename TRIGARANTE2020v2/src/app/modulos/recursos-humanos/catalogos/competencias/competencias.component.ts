import { Component, OnInit } from '@angular/core';
import {CompetenciasService} from '../../../../@core/data/services/rrhh/catalogos-RH/competencias/competencias.service';

@Component({
  selector: 'app-competencias',
  templateUrl: './competencias.component.html',
  styleUrls: ['./competencias.component.scss']
})
export class CompetenciasComponent implements OnInit {

  constructor(private _CompetenciasService: CompetenciasService) { }

  ngOnInit(): void {
  }

}
