import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreEmpleadosComponent } from './pre-empleados.component';

describe('PreEmpleadosComponent', () => {
  let component: PreEmpleadosComponent;
  let fixture: ComponentFixture<PreEmpleadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreEmpleadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreEmpleadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
