import { Component, OnInit } from '@angular/core';
import {EmpleadosService} from '../../../../../@core/data/services/rrhh/empleados/empleados.service';

@Component({
  selector: 'app-empleado-baja',
  templateUrl: './empleado-baja.component.html',
  styleUrls: ['./empleado-baja.component.scss']
})
export class EmpleadoBajaComponent implements OnInit {

  constructor(private _EmpleadosService: EmpleadosService) { }

  ngOnInit(): void {
  }

}
