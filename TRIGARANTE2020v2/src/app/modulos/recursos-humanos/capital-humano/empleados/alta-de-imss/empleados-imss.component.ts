import { Component, OnInit } from '@angular/core';
import {EmpleadosService} from '../../../../../@core/data/services/rrhh/empleados/empleados.service';

@Component({
  selector: 'app-empleados-imss',
  templateUrl: './empleados-imss.component.html',
  styleUrls: ['./empleados-imss.component.scss']
})
export class EmpleadosImssComponent implements OnInit {

  constructor(private _EmpleadosService: EmpleadosService) { }

  ngOnInit(): void {
  }

}
