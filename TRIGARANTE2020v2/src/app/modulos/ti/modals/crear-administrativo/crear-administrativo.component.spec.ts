import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearAdministrativoComponent } from './crear-administrativo.component';

describe('CrearAdministrativoComponent', () => {
  let component: CrearAdministrativoComponent;
  let fixture: ComponentFixture<CrearAdministrativoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearAdministrativoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearAdministrativoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
