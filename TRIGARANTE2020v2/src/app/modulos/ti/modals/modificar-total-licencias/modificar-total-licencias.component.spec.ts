import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarTotalLicenciasComponent } from './modificar-total-licencias.component';

describe('ModificarTotalLicenciasComponent', () => {
  let component: ModificarTotalLicenciasComponent;
  let fixture: ComponentFixture<ModificarTotalLicenciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificarTotalLicenciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarTotalLicenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
