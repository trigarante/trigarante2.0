import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrupoPermisosComponent } from './grupo-permisos.component';

describe('GrupoPermisosComponent', () => {
  let component: GrupoPermisosComponent;
  let fixture: ComponentFixture<GrupoPermisosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrupoPermisosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrupoPermisosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
