import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteGrupoPermisosComponent } from './reporte-grupo-permisos.component';

describe('ReporteGrupoPermisosComponent', () => {
  let component: ReporteGrupoPermisosComponent;
  let fixture: ComponentFixture<ReporteGrupoPermisosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteGrupoPermisosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteGrupoPermisosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
