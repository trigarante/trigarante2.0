import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TiRoutingModule } from './ti-routing.module';
import { TiComponent } from './ti.component';
import { ReporteGrupoPermisosComponent } from './administracion-Ti/reportes/reporte-grupo-permisos/reporte-grupo-permisos.component';
import { ReporteUsuariosComponent } from './administracion-Ti/reportes/reporte-usuarios/reporte-usuarios.component';
import { ReporteExtensionesComponent } from './administracion-Ti/reportes/reporte-extensiones/reporte-extensiones.component';
import { ExtensionesComponent } from './administracion-Ti/extensiones/extensiones.component';
import { UsuariosComponent } from './administracion-Ti/usuarios/usuarios.component';
import { GrupoPermisosComponent } from './administracion-Ti/grupo-permisos/grupo-permisos.component';
import { ActualzarPermisosComponent } from './actualzar-permisos/actualzar-permisos.component';
import { AdministrativoComponent } from './administrativo/administrativo.component';
import { ControlLlamadasComponent } from './control-llamadas/control-llamadas.component';
import { CoordinadorComponent } from './coordinador/coordinador.component';
import { CrearPermisosComponent } from './crear-permisos/crear-permisos.component';
import { EjecutivoComponent } from './ejecutivo/ejecutivo.component';
import { ElminiarPermisosComponent } from './elminiar-permisos/elminiar-permisos.component';
import { GerenteComponent } from './gerente/gerente.component';
import { ReactivarPermisosComponent } from './administracion-Ti/reactivar-permisos/reactivar-permisos.component';
import { PermisosComponent } from './permisos/permisos.component';
import { SupervisorComponent } from './supervisor/supervisor.component';
import { AsignarCorreoComponent } from './modals/asignar-correo/asignar-correo.component';
import { CrearAdministrativoComponent } from './modals/crear-administrativo/crear-administrativo.component';
import { CrearAuditoriaComponent } from './modals/crear-auditoria/crear-auditoria.component';
import { CrearCoordinadorComponent } from './modals/crear-coordinador/crear-coordinador.component';
import { CrearCorreoComponent } from './modals/crear-correo/crear-correo.component';
import { CrearCpuComponent } from './modals/crear-cpu/crear-cpu.component';
import { CrearEjecutivoComponent } from './modals/crear-ejecutivo/crear-ejecutivo.component';
import { CrearExtensionComponent } from './modals/crear-extension/crear-extension.component';
import { CrearGerenteComponent } from './modals/crear-gerente/crear-gerente.component';
import { CrearModoSeguroComponent } from './modals/crear-modo-seguro/crear-modo-seguro.component';
import { CrearSupervisorComponent } from './modals/crear-supervisor/crear-supervisor.component';
import { CrearUsuarioComponent } from './modals/crear-usuario/crear-usuario.component';
import { DatosUsuarioComponent } from './modals/datos-usuario/datos-usuario.component';
import { ExtensionSaveComponent } from './modals/extension-save/extension-save.component';
import { ModificarExtensionComponent } from './modals/modificar-extension/modificar-extension.component';
import { ModificarPermisosComponent } from './modals/modificar-permisos/modificar-permisos.component';
import { ModificarTotalLicenciasComponent } from './modals/modificar-total-licencias/modificar-total-licencias.component';
import { SaveComponent } from './modals/save/save.component';
import { SaveModificadoComponent } from './modals/save-modificado/save-modificado.component';
import {ModificarAliasComponent} from './modals/modificar-alias/modificar-alias.component';


@NgModule({
  declarations: [
    TiComponent,
    ReporteGrupoPermisosComponent,
    ReporteUsuariosComponent,
    ReporteExtensionesComponent,
    ExtensionesComponent,
    UsuariosComponent,
    GrupoPermisosComponent,
    ActualzarPermisosComponent,
    AdministrativoComponent,
    ControlLlamadasComponent,
    CoordinadorComponent,
    CrearPermisosComponent,
    EjecutivoComponent,
    ElminiarPermisosComponent,
    GerenteComponent,
    ReactivarPermisosComponent,
    PermisosComponent,
    SupervisorComponent,
    AsignarCorreoComponent,
    CrearAdministrativoComponent,
    CrearAuditoriaComponent,
    CrearCoordinadorComponent,
    CrearCorreoComponent,
    CrearCpuComponent,
    CrearEjecutivoComponent,
    CrearExtensionComponent,
    CrearGerenteComponent,
    CrearModoSeguroComponent,
    CrearSupervisorComponent,
    CrearUsuarioComponent,
    DatosUsuarioComponent,
    ExtensionSaveComponent,
    ModificarAliasComponent,
    ModificarExtensionComponent,
    ModificarPermisosComponent,
    ModificarTotalLicenciasComponent,
    SaveComponent,
    SaveModificadoComponent


  ],
  imports: [
    CommonModule,
    TiRoutingModule
  ]
})
export class TiModule { }
