import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualzarPermisosComponent } from './actualzar-permisos.component';

describe('ActualzarPermisosComponent', () => {
  let component: ActualzarPermisosComponent;
  let fixture: ComponentFixture<ActualzarPermisosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualzarPermisosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualzarPermisosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
