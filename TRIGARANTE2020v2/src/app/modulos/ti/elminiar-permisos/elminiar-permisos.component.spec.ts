import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElminiarPermisosComponent } from './elminiar-permisos.component';

describe('ElminiarPermisosComponent', () => {
  let component: ElminiarPermisosComponent;
  let fixture: ComponentFixture<ElminiarPermisosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElminiarPermisosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElminiarPermisosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
