import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminsitracionTicketsComponent } from './adminsitracion-tickets.component';

describe('AdminsitracionTicketsComponent', () => {
  let component: AdminsitracionTicketsComponent;
  let fixture: ComponentFixture<AdminsitracionTicketsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminsitracionTicketsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminsitracionTicketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
