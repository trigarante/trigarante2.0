import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivacionEmpleadosComponent } from './activacion-empleados.component';

describe('ActivacionEmpleadosComponent', () => {
  let component: ActivacionEmpleadosComponent;
  let fixture: ComponentFixture<ActivacionEmpleadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivacionEmpleadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivacionEmpleadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
