import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalificarTicketComponent } from './calificar-ticket.component';

describe('CalificarTicketComponent', () => {
  let component: CalificarTicketComponent;
  let fixture: ComponentFixture<CalificarTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalificarTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalificarTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
