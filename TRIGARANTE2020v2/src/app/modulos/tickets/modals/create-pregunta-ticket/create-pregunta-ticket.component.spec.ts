import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePreguntaTicketComponent } from './create-pregunta-ticket.component';

describe('CreatePreguntaTicketComponent', () => {
  let component: CreatePreguntaTicketComponent;
  let fixture: ComponentFixture<CreatePreguntaTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePreguntaTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePreguntaTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
