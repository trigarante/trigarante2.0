import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOpcionComponent } from './create-opcion.component';

describe('CreateOpcionComponent', () => {
  let component: CreateOpcionComponent;
  let fixture: ComponentFixture<CreateOpcionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOpcionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOpcionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
