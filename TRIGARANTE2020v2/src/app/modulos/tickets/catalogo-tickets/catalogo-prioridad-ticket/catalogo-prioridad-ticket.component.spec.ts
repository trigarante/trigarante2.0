import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogoPrioridadTicketComponent } from './catalogo-prioridad-ticket.component';

describe('CatalogoPrioridadTicketComponent', () => {
  let component: CatalogoPrioridadTicketComponent;
  let fixture: ComponentFixture<CatalogoPrioridadTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogoPrioridadTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogoPrioridadTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
