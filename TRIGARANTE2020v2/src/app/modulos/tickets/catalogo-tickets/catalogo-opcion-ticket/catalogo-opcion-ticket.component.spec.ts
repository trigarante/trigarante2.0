import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogoOpcionTicketComponent } from './catalogo-opcion-ticket.component';

describe('CatalogoOpcionTicketComponent', () => {
  let component: CatalogoOpcionTicketComponent;
  let fixture: ComponentFixture<CatalogoOpcionTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogoOpcionTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogoOpcionTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
