import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogoEstadoPreguntaComponent } from './catalogo-estado-pregunta.component';

describe('CatalogoEstadoPreguntaComponent', () => {
  let component: CatalogoEstadoPreguntaComponent;
  let fixture: ComponentFixture<CatalogoEstadoPreguntaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogoEstadoPreguntaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogoEstadoPreguntaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
