import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogoRespuestaTicketComponent } from './catalogo-respuesta-ticket.component';

describe('CatalogoRespuestaTicketComponent', () => {
  let component: CatalogoRespuestaTicketComponent;
  let fixture: ComponentFixture<CatalogoRespuestaTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogoRespuestaTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogoRespuestaTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
