import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogoEstadoTicketComponent } from './catalogo-estado-ticket.component';

describe('CatalogoEstadoTicketComponent', () => {
  let component: CatalogoEstadoTicketComponent;
  let fixture: ComponentFixture<CatalogoEstadoTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogoEstadoTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogoEstadoTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
