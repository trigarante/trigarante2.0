import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogoTipoTicketComponent } from './catalogo-tipo-ticket.component';

describe('CatalogoTipoTicketComponent', () => {
  let component: CatalogoTipoTicketComponent;
  let fixture: ComponentFixture<CatalogoTipoTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogoTipoTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogoTipoTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
