import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogoMotivoTicketComponent } from './catalogo-motivo-ticket.component';

describe('CatalogoMotivoTicketComponent', () => {
  let component: CatalogoMotivoTicketComponent;
  let fixture: ComponentFixture<CatalogoMotivoTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogoMotivoTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogoMotivoTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
