import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketsCerradosComponent } from './tickets-cerrados.component';

describe('TicketsCerradosComponent', () => {
  let component: TicketsCerradosComponent;
  let fixture: ComponentFixture<TicketsCerradosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketsCerradosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketsCerradosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
