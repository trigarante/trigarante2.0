import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketsAbiertosComponent } from './tickets-abiertos.component';

describe('TicketsAbiertosComponent', () => {
  let component: TicketsAbiertosComponent;
  let fixture: ComponentFixture<TicketsAbiertosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketsAbiertosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketsAbiertosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
