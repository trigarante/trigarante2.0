import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RetencionRoutingModule } from './retencion-routing.module';
import { RetencionComponent } from './retencion.component';
import { AdministracionPolizasComponent } from './administracion-polizas/administracion-polizas.component';
import { DetallesPolizaComponent } from './detalles-poliza/detalles-poliza.component';
import { EndosoCreateComponent } from './modals/endoso-create/endoso-create.component';
import { PagosRecibosComponent } from './pagos-recibos/pagos-recibos.component';
import { RecibosComponent } from './recibos/recibos.component';
import { ViewerPolizaComponent } from './viewer-poliza/viewer-poliza.component';


@NgModule({
  declarations: [RetencionComponent, AdministracionPolizasComponent, DetallesPolizaComponent, EndosoCreateComponent, PagosRecibosComponent, RecibosComponent, ViewerPolizaComponent],
  imports: [
    CommonModule,
    RetencionRoutingModule
  ]
})
export class RetencionModule { }
