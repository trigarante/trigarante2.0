import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagosRecibosComponent } from './pagos-recibos.component';

describe('PagosRecibosComponent', () => {
  let component: PagosRecibosComponent;
  let fixture: ComponentFixture<PagosRecibosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagosRecibosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagosRecibosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
