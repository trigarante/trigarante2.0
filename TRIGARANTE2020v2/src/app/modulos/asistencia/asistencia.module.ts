import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AsistenciaRoutingModule } from './asistencia-routing.module';
import { VerAsistenciaComponent } from './ver-asistencia/ver-asistencia.component';
import {AsistenciaComponent} from './asistencia.component';


@NgModule({
  imports: [
    CommonModule,
    AsistenciaRoutingModule
  ],
  declarations: [
    VerAsistenciaComponent,
    AsistenciaComponent

  ]
})
export class AsistenciaModule { }
