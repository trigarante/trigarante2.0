import { Component, OnInit } from '@angular/core';
import {ConveniosService} from '../../../@core/data/services/comerciales/convenios.service';

@Component({
  selector: 'app-convenios',
  templateUrl: './convenios.component.html',
  styleUrls: ['./convenios.component.scss']
})
export class ConveniosComponent implements OnInit {

  constructor(private _ConveniosService: ConveniosService) { }

  ngOnInit(): void {
  }

}
