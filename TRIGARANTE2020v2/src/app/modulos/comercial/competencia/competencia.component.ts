import { Component, OnInit } from '@angular/core';
import {CompetenciasSociosService} from '../../../@core/data/services/comerciales/competencias-socios.service';

@Component({
  selector: 'app-competencia',
  templateUrl: './competencia.component.html',
  styleUrls: ['./competencia.component.scss']
})
export class CompetenciaComponent implements OnInit {

  constructor(private _CompetenciasSociosService: CompetenciasSociosService) { }

  ngOnInit(): void {
  }

}
