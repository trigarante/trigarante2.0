import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComercialRoutingModule } from './comercial-routing.module';
import { ComercialComponent } from './comercial.component';
import { AnalisisCompetenciaComponent } from './analisis-competencia/analisis-competencia.component';
import { AsignarUrlComponent } from './asignar-url/asignar-url.component';
import { CoberturaComponent } from './cobertura/cobertura.component';
import { CompetenciaComponent } from './competencia/competencia.component';
import { ContratosComponent } from './contratos/contratos.component';
import { ConveniosComponent } from './convenios/convenios.component';
import { DocumentosComercialComponent } from './documentos-comercial/documentos-comercial.component';
import { EjecutivoCuentaComponent } from './ejecutivo-cuenta/ejecutivo-cuenta.component';
import { PresupuestoComponent } from './presupuesto/presupuesto.component';
import { ProductoComponent } from './producto/producto.component';
import { RamoComponent } from './ramo/ramo.component';
import { SociosComponent } from './socios/socios.component';
import { SubRamoComponent } from './sub-ramo/sub-ramo.component';
import { AsignarAnalisisCompetenciaComponent } from './modals/asignar-analisis-competencia/asignar-analisis-competencia.component';
import { AsignarCoberturaComponent } from './modals/asignar-cobertura/asignar-cobertura.component';
import { AsignarCompetenciaComponent } from './modals/asignar-competencia/asignar-competencia.component';
import { AsignarContratosComponent } from './modals/asignar-contratos/asignar-contratos.component';
import { AsignarConveniosComponent } from './modals/asignar-convenios/asignar-convenios.component';
import { AsignarPresupuestoComponent } from './modals/asignar-presupuesto/asignar-presupuesto.component';
import { AsignarProductoComponent } from './modals/asignar-producto/asignar-producto.component';
import { AsignarRamoComponent } from './modals/asignar-ramo/asignar-ramo.component';
import { AsignarSubramoComponent } from './modals/asignar-subramo/asignar-subramo.component';
import { DatosAnalisisCompetenciaComponent } from './modals/datos-analisis-competencia/datos-analisis-competencia.component';
import { DatosCoberturaComponent } from './modals/datos-cobertura/datos-cobertura.component';
import { DatosCompetenciaComponent } from './modals/datos-competencia/datos-competencia.component';
import { DatosConveniosComponent } from './modals/datos-convenios/datos-convenios.component';
import { DatosEjecutivoCuentaComponent } from './modals/datos-ejecutivo-cuenta/datos-ejecutivo-cuenta.component';
import { DatosProductoComponent } from './modals/datos-producto/datos-producto.component';
import { DatosRamoComponent } from './modals/datos-ramo/datos-ramo.component';
import { DatosSociosComponent } from './modals/datos-socios/datos-socios.component';
import { DatosSubramoComponent } from './modals/datos-subramo/datos-subramo.component';
import { EjecutivoCuentaCreateComponent } from './modals/ejecutivo-cuenta-create/ejecutivo-cuenta-create.component';
import { SociosCreateComponent } from './modals/socios-create/socios-create.component';
import { UpdateBajaSociosComponent } from './modals/update-baja-socios/update-baja-socios.component';
import { UrlCreateComponent } from './modals/url-create/url-create.component';
import {AnalisisCompetenciasService} from '../../@core/data/services/comerciales/analisis-competencias.service';
import {AsignarUrlService} from '../../@core/data/services/comerciales/asignar-url.service';
import {CoberturaService} from '../../@core/data/services/comerciales/cobertura.service';
import {CompetenciasSociosService} from '../../@core/data/services/comerciales/competencias-socios.service';
import {ContratosService} from '../../@core/data/services/comerciales/contratos.service';
import {ConveniosService} from '../../@core/data/services/comerciales/convenios.service';
import {EjecutivoCuentaService} from '../../@core/data/services/comerciales/ejecutivo-cuenta.service';
import {PresupuestoService} from '../../@core/data/services/comerciales/presupuesto.service';
import {ProductoSociosService} from '../../@core/data/services/comerciales/producto-socios.service';
import {RamoService} from '../../@core/data/services/comerciales/ramo.service';
import {SociosService} from '../../@core/data/services/comerciales/socios.service';
import {SubRamoService} from '../../@core/data/services/comerciales/sub-ramo.service';
import {DocumentosComercialService} from '../../@core/data/services/comerciales/documentos-comercial.service';
import { ReporteSocioComponent } from './reportes/reporte-socio/reporte-socio.component';


@NgModule({
  declarations: [
    ComercialComponent,
    AnalisisCompetenciaComponent,
    AsignarUrlComponent,
    CoberturaComponent,
    CompetenciaComponent,
    ContratosComponent,
    ConveniosComponent,
    DocumentosComercialComponent,
    EjecutivoCuentaComponent,
    PresupuestoComponent,
    ProductoComponent,
    RamoComponent,
    SociosComponent,
    SubRamoComponent,
    AsignarAnalisisCompetenciaComponent,
    AsignarCoberturaComponent,
    AsignarCompetenciaComponent,
    AsignarContratosComponent,
    AsignarConveniosComponent,
    AsignarPresupuestoComponent,
    AsignarProductoComponent,
    AsignarRamoComponent,
    AsignarSubramoComponent,
    DatosAnalisisCompetenciaComponent,
    DatosCoberturaComponent,
    DatosCompetenciaComponent,
    DatosConveniosComponent,
    DatosEjecutivoCuentaComponent,
    DatosProductoComponent,
    DatosRamoComponent,
    DatosSociosComponent,
    DatosSubramoComponent,
    EjecutivoCuentaCreateComponent,
    SociosCreateComponent,
    UpdateBajaSociosComponent,
    UrlCreateComponent,
    ReporteSocioComponent

  ],
  imports: [
    CommonModule,
    ComercialRoutingModule
  ],

  providers: [
    AnalisisCompetenciasService,
    AsignarUrlService,
    CoberturaService,
    CompetenciasSociosService,
    ContratosService,
    ConveniosService,
    EjecutivoCuentaService,
    PresupuestoService,
    ProductoSociosService,
    RamoService,
    SociosService,
    SubRamoService,
    DocumentosComercialService

  ],
})
export class ComercialModule { }
