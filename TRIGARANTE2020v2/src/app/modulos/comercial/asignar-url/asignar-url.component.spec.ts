import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarUrlComponent } from './asignar-url.component';

describe('AsignarUrlComponent', () => {
  let component: AsignarUrlComponent;
  let fixture: ComponentFixture<AsignarUrlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarUrlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarUrlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
