import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EjecutivoCuentaComponent } from './ejecutivo-cuenta.component';

describe('EjecutivoCuentaComponent', () => {
  let component: EjecutivoCuentaComponent;
  let fixture: ComponentFixture<EjecutivoCuentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EjecutivoCuentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EjecutivoCuentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
