import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentosComercialComponent } from './documentos-comercial.component';

describe('DocumentosComercialComponent', () => {
  let component: DocumentosComercialComponent;
  let fixture: ComponentFixture<DocumentosComercialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentosComercialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentosComercialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
