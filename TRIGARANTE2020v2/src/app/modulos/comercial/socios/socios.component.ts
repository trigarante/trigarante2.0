import { Component, OnInit } from '@angular/core';
import {SociosService} from '../../../@core/data/services/comerciales/socios.service';

@Component({
  selector: 'app-socios',
  templateUrl: './socios.component.html',
  styleUrls: ['./socios.component.scss']
})
export class SociosComponent implements OnInit {

  constructor(private _SociosService: SociosService) { }

  ngOnInit(): void {
  }

}
