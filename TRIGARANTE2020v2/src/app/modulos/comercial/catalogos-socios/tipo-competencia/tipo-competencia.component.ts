import { Component, OnInit } from '@angular/core';
import {TipoCompetenciaService} from '../../../../@core/data/services/comerciales/catalogo/tipo-competencia.service';

@Component({
  selector: 'app-tipo-competencia',
  templateUrl: './tipo-competencia.component.html',
  styleUrls: ['./tipo-competencia.component.scss']
})
export class TipoCompetenciaComponent implements OnInit {

  constructor(private _TipoCompetenciaService: TipoCompetenciaService) { }

  ngOnInit(): void {
  }

}
