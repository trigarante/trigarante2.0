import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoCompetenciaComponent } from './tipo-competencia.component';

describe('TipoCompetenciaComponent', () => {
  let component: TipoCompetenciaComponent;
  let fixture: ComponentFixture<TipoCompetenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoCompetenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoCompetenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
