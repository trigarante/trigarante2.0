import { Component, OnInit } from '@angular/core';
import {DivisasService} from '../../../../@core/data/services/comerciales/catalogo/divisas.service';

@Component({
  selector: 'app-divisas',
  templateUrl: './divisas.component.html',
  styleUrls: ['./divisas.component.scss']
})
export class DivisasComponent implements OnInit {

  constructor(private _DivisasService: DivisasService) { }

  ngOnInit(): void {
  }

}
