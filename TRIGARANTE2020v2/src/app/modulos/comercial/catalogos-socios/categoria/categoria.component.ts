import { Component, OnInit } from '@angular/core';
import {CategoriaService} from '../../../../@core/data/services/comerciales/catalogo/categoria.service';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.scss']
})
export class CategoriaComponent implements OnInit {

  constructor(private _CategoriaService: CategoriaService) { }

  ngOnInit(): void {
  }

}
