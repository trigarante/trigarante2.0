import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BajaSociosComponent } from './baja-socios.component';

describe('BajaSociosComponent', () => {
  let component: BajaSociosComponent;
  let fixture: ComponentFixture<BajaSociosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BajaSociosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BajaSociosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
