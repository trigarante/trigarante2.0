import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DivisaCreateComponent } from './divisa-create.component';

describe('DivisaCreateComponent', () => {
  let component: DivisaCreateComponent;
  let fixture: ComponentFixture<DivisaCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DivisaCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DivisaCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
