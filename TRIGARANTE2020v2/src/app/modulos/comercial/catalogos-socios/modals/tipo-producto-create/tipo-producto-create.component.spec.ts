import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoProductoCreateComponent } from './tipo-producto-create.component';

describe('TipoProductoCreateComponent', () => {
  let component: TipoProductoCreateComponent;
  let fixture: ComponentFixture<TipoProductoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoProductoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoProductoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
