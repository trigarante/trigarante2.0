import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoCategoriaCreateComponent } from './tipo-categoria-create.component';

describe('TipoCategoriaCreateComponent', () => {
  let component: TipoCategoriaCreateComponent;
  let fixture: ComponentFixture<TipoCategoriaCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoCategoriaCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoCategoriaCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
