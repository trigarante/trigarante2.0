import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoSubramoCreateComponent } from './tipo-subramo-create.component';

describe('TipoSubramoCreateComponent', () => {
  let component: TipoSubramoCreateComponent;
  let fixture: ComponentFixture<TipoSubramoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoSubramoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoSubramoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
