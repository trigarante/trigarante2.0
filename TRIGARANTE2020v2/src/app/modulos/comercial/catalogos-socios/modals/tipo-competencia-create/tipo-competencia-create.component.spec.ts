import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoCompetenciaCreateComponent } from './tipo-competencia-create.component';

describe('TipoCompetenciaCreateComponent', () => {
  let component: TipoCompetenciaCreateComponent;
  let fixture: ComponentFixture<TipoCompetenciaCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoCompetenciaCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoCompetenciaCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
