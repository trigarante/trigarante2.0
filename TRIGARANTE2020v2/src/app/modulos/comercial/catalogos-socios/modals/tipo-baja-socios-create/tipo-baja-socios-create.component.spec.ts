import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoBajaSociosCreateComponent } from './tipo-baja-socios-create.component';

describe('TipoBajaSociosCreateComponent', () => {
  let component: TipoBajaSociosCreateComponent;
  let fixture: ComponentFixture<TipoBajaSociosCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoBajaSociosCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoBajaSociosCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
