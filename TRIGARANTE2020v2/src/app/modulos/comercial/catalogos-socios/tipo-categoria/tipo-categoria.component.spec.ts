import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoCategoriaComponent } from './tipo-categoria.component';

describe('TipoCategoriaComponent', () => {
  let component: TipoCategoriaComponent;
  let fixture: ComponentFixture<TipoCategoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoCategoriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoCategoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
