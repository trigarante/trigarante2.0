import { Component, OnInit } from '@angular/core';
import {AnalisisCompetenciasService} from '../../../@core/data/services/comerciales/analisis-competencias.service';

@Component({
  selector: 'app-analisis-competencia',
  templateUrl: './analisis-competencia.component.html',
  styleUrls: ['./analisis-competencia.component.scss']
})
export class AnalisisCompetenciaComponent implements OnInit {

  // @ts-ignore
  constructor(private _AnalisisCompetenciasService: AnalisisCompetenciasService) { }

  ngOnInit(): void {
  }

}
