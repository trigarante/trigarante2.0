import { Component, OnInit } from '@angular/core';
import {CoberturaService} from '../../../@core/data/services/comerciales/cobertura.service';

@Component({
  selector: 'app-cobertura',
  templateUrl: './cobertura.component.html',
  styleUrls: ['./cobertura.component.scss']
})
export class CoberturaComponent implements OnInit {

  constructor(private _CoberturaService: CoberturaService) { }

  ngOnInit(): void {
  }

}
