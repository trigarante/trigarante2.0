import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosCompetenciaComponent } from './datos-competencia.component';

describe('DatosCompetenciaComponent', () => {
  let component: DatosCompetenciaComponent;
  let fixture: ComponentFixture<DatosCompetenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosCompetenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosCompetenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
