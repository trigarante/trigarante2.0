import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EjecutivoCuentaCreateComponent } from './ejecutivo-cuenta-create.component';

describe('EjecutivoCuentaCreateComponent', () => {
  let component: EjecutivoCuentaCreateComponent;
  let fixture: ComponentFixture<EjecutivoCuentaCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EjecutivoCuentaCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EjecutivoCuentaCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
