import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarRamoComponent } from './asignar-ramo.component';

describe('AsignarRamoComponent', () => {
  let component: AsignarRamoComponent;
  let fixture: ComponentFixture<AsignarRamoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarRamoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarRamoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
