import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarContratosComponent } from './asignar-contratos.component';

describe('AsignarContratosComponent', () => {
  let component: AsignarContratosComponent;
  let fixture: ComponentFixture<AsignarContratosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarContratosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarContratosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
