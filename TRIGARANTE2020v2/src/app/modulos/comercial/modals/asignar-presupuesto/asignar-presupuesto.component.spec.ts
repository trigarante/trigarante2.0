import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarPresupuestoComponent } from './asignar-presupuesto.component';

describe('AsignarPresupuestoComponent', () => {
  let component: AsignarPresupuestoComponent;
  let fixture: ComponentFixture<AsignarPresupuestoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarPresupuestoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarPresupuestoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
