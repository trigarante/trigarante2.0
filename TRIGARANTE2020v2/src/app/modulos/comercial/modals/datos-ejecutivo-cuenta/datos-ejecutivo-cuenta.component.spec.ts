import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosEjecutivoCuentaComponent } from './datos-ejecutivo-cuenta.component';

describe('DatosEjecutivoCuentaComponent', () => {
  let component: DatosEjecutivoCuentaComponent;
  let fixture: ComponentFixture<DatosEjecutivoCuentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosEjecutivoCuentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosEjecutivoCuentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
