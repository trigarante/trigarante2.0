import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarConveniosComponent } from './asignar-convenios.component';

describe('AsignarConveniosComponent', () => {
  let component: AsignarConveniosComponent;
  let fixture: ComponentFixture<AsignarConveniosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarConveniosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarConveniosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
