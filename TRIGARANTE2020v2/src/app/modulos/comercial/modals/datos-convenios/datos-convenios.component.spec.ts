import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosConveniosComponent } from './datos-convenios.component';

describe('DatosConveniosComponent', () => {
  let component: DatosConveniosComponent;
  let fixture: ComponentFixture<DatosConveniosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosConveniosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosConveniosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
