import { Component, OnInit } from '@angular/core';
import {RamoService} from '../../../@core/data/services/comerciales/ramo.service';

@Component({
  selector: 'app-ramo',
  templateUrl: './ramo.component.html',
  styleUrls: ['./ramo.component.scss']
})
export class RamoComponent implements OnInit {

  constructor(private _RamoService: RamoService) { }

  ngOnInit(): void {
  }

}
