import { Component, OnInit } from '@angular/core';
import {SubRamoService} from '../../../@core/data/services/comerciales/sub-ramo.service';

@Component({
  selector: 'app-sub-ramo',
  templateUrl: './sub-ramo.component.html',
  styleUrls: ['./sub-ramo.component.scss']
})
export class SubRamoComponent implements OnInit {

  constructor(private _SubRamoService: SubRamoService) { }

  ngOnInit(): void {
  }

}
