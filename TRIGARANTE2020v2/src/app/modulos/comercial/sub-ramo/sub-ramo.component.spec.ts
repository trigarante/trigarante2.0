import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubRamoComponent } from './sub-ramo.component';

describe('SubRamoComponent', () => {
  let component: SubRamoComponent;
  let fixture: ComponentFixture<SubRamoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubRamoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubRamoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
