import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpresaFinanzasComponent } from './empresa-finanzas.component';

describe('EmpresaFinanzasComponent', () => {
  let component: EmpresaFinanzasComponent;
  let fixture: ComponentFixture<EmpresaFinanzasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpresaFinanzasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpresaFinanzasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
