import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreasFinanzasComponent } from './areas-finanzas.component';

describe('AreasFinanzasComponent', () => {
  let component: AreasFinanzasComponent;
  let fixture: ComponentFixture<AreasFinanzasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreasFinanzasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreasFinanzasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
