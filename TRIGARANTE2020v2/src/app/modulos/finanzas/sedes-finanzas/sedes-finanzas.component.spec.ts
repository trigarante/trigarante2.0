import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SedesFinanzasComponent } from './sedes-finanzas.component';

describe('SedesFinanzasComponent', () => {
  let component: SedesFinanzasComponent;
  let fixture: ComponentFixture<SedesFinanzasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SedesFinanzasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SedesFinanzasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
