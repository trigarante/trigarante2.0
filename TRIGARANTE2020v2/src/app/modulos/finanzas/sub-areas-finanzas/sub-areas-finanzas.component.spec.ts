import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubAreasFinanzasComponent } from './sub-areas-finanzas.component';

describe('SubAreasFinanzasComponent', () => {
  let component: SubAreasFinanzasComponent;
  let fixture: ComponentFixture<SubAreasFinanzasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubAreasFinanzasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubAreasFinanzasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
