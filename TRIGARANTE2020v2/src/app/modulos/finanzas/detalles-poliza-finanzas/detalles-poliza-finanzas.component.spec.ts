import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesPolizaFinanzasComponent } from './detalles-poliza-finanzas.component';

describe('DetallesPolizaFinanzasComponent', () => {
  let component: DetallesPolizaFinanzasComponent;
  let fixture: ComponentFixture<DetallesPolizaFinanzasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallesPolizaFinanzasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesPolizaFinanzasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
