import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CotizadorInternoSubSecuentesComponent } from './cotizador-interno-sub-secuentes.component';

describe('CotizadorInternoSubSecuentesComponent', () => {
  let component: CotizadorInternoSubSecuentesComponent;
  let fixture: ComponentFixture<CotizadorInternoSubSecuentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CotizadorInternoSubSecuentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CotizadorInternoSubSecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
