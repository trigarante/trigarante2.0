import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CotizadorTabsSubSecuentesComponent } from './cotizador-tabs-sub-secuentes.component';

describe('CotizadorTabsSubSecuentesComponent', () => {
  let component: CotizadorTabsSubSecuentesComponent;
  let fixture: ComponentFixture<CotizadorTabsSubSecuentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CotizadorTabsSubSecuentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CotizadorTabsSubSecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
