import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolizasTotalesComponent } from './polizas-totales.component';

describe('PolizasTotalesComponent', () => {
  let component: PolizasTotalesComponent;
  let fixture: ComponentFixture<PolizasTotalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolizasTotalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolizasTotalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
