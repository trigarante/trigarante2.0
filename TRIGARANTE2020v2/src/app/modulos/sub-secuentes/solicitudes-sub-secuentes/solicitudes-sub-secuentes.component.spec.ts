import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudesSubSecuentesComponent } from './solicitudes-sub-secuentes.component';

describe('SolicitudesSubSecuentesComponent', () => {
  let component: SolicitudesSubSecuentesComponent;
  let fixture: ComponentFixture<SolicitudesSubSecuentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudesSubSecuentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudesSubSecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
