import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorreccionErroresPolizaComponent } from './correccion-errores-poliza.component';

describe('CorreccionErroresPolizaComponent', () => {
  let component: CorreccionErroresPolizaComponent;
  let fixture: ComponentFixture<CorreccionErroresPolizaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorreccionErroresPolizaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorreccionErroresPolizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
