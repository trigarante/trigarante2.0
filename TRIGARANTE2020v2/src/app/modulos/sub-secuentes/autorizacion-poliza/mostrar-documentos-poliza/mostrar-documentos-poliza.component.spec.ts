import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarDocumentosPolizaComponent } from './mostrar-documentos-poliza.component';

describe('MostrarDocumentosPolizaComponent', () => {
  let component: MostrarDocumentosPolizaComponent;
  let fixture: ComponentFixture<MostrarDocumentosPolizaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostrarDocumentosPolizaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarDocumentosPolizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
