import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentosCorreccionPolizaComponent } from './documentos-correccion-poliza.component';

describe('DocumentosCorreccionPolizaComponent', () => {
  let component: DocumentosCorreccionPolizaComponent;
  let fixture: ComponentFixture<DocumentosCorreccionPolizaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentosCorreccionPolizaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentosCorreccionPolizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
