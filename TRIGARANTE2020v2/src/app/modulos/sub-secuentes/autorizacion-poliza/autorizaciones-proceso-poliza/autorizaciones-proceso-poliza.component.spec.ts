import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorizacionesProcesoPolizaComponent } from './autorizaciones-proceso-poliza.component';

describe('AutorizacionesProcesoPolizaComponent', () => {
  let component: AutorizacionesProcesoPolizaComponent;
  let fixture: ComponentFixture<AutorizacionesProcesoPolizaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutorizacionesProcesoPolizaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorizacionesProcesoPolizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
