import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LlamadasMyccSubsComponent } from './llamadas-mycc-subs.component';

describe('LlamadasMyccSubsComponent', () => {
  let component: LlamadasMyccSubsComponent;
  let fixture: ComponentFixture<LlamadasMyccSubsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LlamadasMyccSubsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LlamadasMyccSubsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
