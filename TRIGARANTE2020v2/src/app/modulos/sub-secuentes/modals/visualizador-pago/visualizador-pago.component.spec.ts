import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizadorPagoComponent } from './visualizador-pago.component';

describe('VisualizadorPagoComponent', () => {
  let component: VisualizadorPagoComponent;
  let fixture: ComponentFixture<VisualizadorPagoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizadorPagoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizadorPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
