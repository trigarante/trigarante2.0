import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosSolicitudSubsComponent } from './datos-solicitud-subs.component';

describe('DatosSolicitudSubsComponent', () => {
  let component: DatosSolicitudSubsComponent;
  let fixture: ComponentFixture<DatosSolicitudSubsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosSolicitudSubsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosSolicitudSubsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
