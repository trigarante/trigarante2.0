import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReasignarRecibosComponent } from './reasignar-recibos.component';

describe('ReasignarRecibosComponent', () => {
  let component: ReasignarRecibosComponent;
  let fixture: ComponentFixture<ReasignarRecibosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReasignarRecibosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReasignarRecibosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
