import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReasifnarSoliciudSubsComponent } from './reasifnar-soliciud-subs.component';

describe('ReasifnarSoliciudSubsComponent', () => {
  let component: ReasifnarSoliciudSubsComponent;
  let fixture: ComponentFixture<ReasifnarSoliciudSubsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReasifnarSoliciudSubsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReasifnarSoliciudSubsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
