import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorizacionesProcesoSubSecuentesComponent } from './autorizaciones-proceso-sub-secuentes.component';

describe('AutorizacionesProcesoSubSecuentesComponent', () => {
  let component: AutorizacionesProcesoSubSecuentesComponent;
  let fixture: ComponentFixture<AutorizacionesProcesoSubSecuentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutorizacionesProcesoSubSecuentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorizacionesProcesoSubSecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
