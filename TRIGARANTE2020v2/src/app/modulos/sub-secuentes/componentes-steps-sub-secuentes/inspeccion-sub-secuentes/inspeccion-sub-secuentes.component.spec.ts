import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspeccionSubSecuentesComponent } from './inspeccion-sub-secuentes.component';

describe('InspeccionSubSecuentesComponent', () => {
  let component: InspeccionSubSecuentesComponent;
  let fixture: ComponentFixture<InspeccionSubSecuentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspeccionSubSecuentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspeccionSubSecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
