import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClienteSubSecuentesComponent } from './cliente-sub-secuentes.component';

describe('ClienteSubSecuentesComponent', () => {
  let component: ClienteSubSecuentesComponent;
  let fixture: ComponentFixture<ClienteSubSecuentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClienteSubSecuentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClienteSubSecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
