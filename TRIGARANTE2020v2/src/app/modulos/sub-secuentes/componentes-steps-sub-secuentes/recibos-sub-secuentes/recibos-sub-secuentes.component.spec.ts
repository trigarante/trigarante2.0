import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecibosSubSecuentesComponent } from './recibos-sub-secuentes.component';

describe('RecibosSubSecuentesComponent', () => {
  let component: RecibosSubSecuentesComponent;
  let fixture: ComponentFixture<RecibosSubSecuentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecibosSubSecuentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecibosSubSecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
