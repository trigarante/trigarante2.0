import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroPolizaSubSecuentesComponent } from './registro-poliza-sub-secuentes.component';

describe('RegistroPolizaSubSecuentesComponent', () => {
  let component: RegistroPolizaSubSecuentesComponent;
  let fixture: ComponentFixture<RegistroPolizaSubSecuentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroPolizaSubSecuentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroPolizaSubSecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
