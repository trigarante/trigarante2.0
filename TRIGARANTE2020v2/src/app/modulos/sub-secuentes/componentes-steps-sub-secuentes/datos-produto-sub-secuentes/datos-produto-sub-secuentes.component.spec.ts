import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosProdutoSubSecuentesComponent } from './datos-produto-sub-secuentes.component';

describe('DatosProdutoSubSecuentesComponent', () => {
  let component: DatosProdutoSubSecuentesComponent;
  let fixture: ComponentFixture<DatosProdutoSubSecuentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosProdutoSubSecuentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosProdutoSubSecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
