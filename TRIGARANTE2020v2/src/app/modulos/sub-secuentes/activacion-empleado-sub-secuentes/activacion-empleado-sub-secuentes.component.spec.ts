import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivacionEmpleadoSubSecuentesComponent } from './activacion-empleado-sub-secuentes.component';

describe('ActivacionEmpleadoSubSecuentesComponent', () => {
  let component: ActivacionEmpleadoSubSecuentesComponent;
  let fixture: ComponentFixture<ActivacionEmpleadoSubSecuentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivacionEmpleadoSubSecuentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivacionEmpleadoSubSecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
