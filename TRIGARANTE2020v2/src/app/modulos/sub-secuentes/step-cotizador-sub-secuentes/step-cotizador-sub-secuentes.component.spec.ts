import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepCotizadorSubSecuentesComponent } from './step-cotizador-sub-secuentes.component';

describe('StepCotizadorSubSecuentesComponent', () => {
  let component: StepCotizadorSubSecuentesComponent;
  let fixture: ComponentFixture<StepCotizadorSubSecuentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepCotizadorSubSecuentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepCotizadorSubSecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
