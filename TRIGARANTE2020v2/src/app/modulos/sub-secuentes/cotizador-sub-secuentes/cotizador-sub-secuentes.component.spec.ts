import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CotizadorSubSecuentesComponent } from './cotizador-sub-secuentes.component';

describe('CotizadorSubSecuentesComponent', () => {
  let component: CotizadorSubSecuentesComponent;
  let fixture: ComponentFixture<CotizadorSubSecuentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CotizadorSubSecuentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CotizadorSubSecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
