import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleSolicitudPymeComponent } from './detalle-solicitud-pyme.component';

describe('DetalleSolicitudPymeComponent', () => {
  let component: DetalleSolicitudPymeComponent;
  let fixture: ComponentFixture<DetalleSolicitudPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleSolicitudPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleSolicitudPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
