import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtiquetaSolicitudPymeComponent } from './etiqueta-solicitud-pyme.component';

describe('EtiquetaSolicitudPymeComponent', () => {
  let component: EtiquetaSolicitudPymeComponent;
  let fixture: ComponentFixture<EtiquetaSolicitudPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtiquetaSolicitudPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtiquetaSolicitudPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
