import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudPymeCreateComponent } from './solicitud-pyme-create.component';

describe('SolicitudPymeCreateComponent', () => {
  let component: SolicitudPymeCreateComponent;
  let fixture: ComponentFixture<SolicitudPymeCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudPymeCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudPymeCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
