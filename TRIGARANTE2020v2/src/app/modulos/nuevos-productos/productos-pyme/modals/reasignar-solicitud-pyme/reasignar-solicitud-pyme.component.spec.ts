import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReasignarSolicitudPymeComponent } from './reasignar-solicitud-pyme.component';

describe('ReasignarSolicitudPymeComponent', () => {
  let component: ReasignarSolicitudPymeComponent;
  let fixture: ComponentFixture<ReasignarSolicitudPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReasignarSolicitudPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReasignarSolicitudPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
