import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorreccionErroresPymeComponent } from './correccion-errores-pyme.component';

describe('CorreccionErroresPymeComponent', () => {
  let component: CorreccionErroresPymeComponent;
  let fixture: ComponentFixture<CorreccionErroresPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorreccionErroresPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorreccionErroresPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
