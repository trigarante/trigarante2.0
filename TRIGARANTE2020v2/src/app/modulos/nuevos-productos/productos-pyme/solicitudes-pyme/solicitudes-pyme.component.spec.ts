import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudesPymeComponent } from './solicitudes-pyme.component';

describe('SolicitudesPymeComponent', () => {
  let component: SolicitudesPymeComponent;
  let fixture: ComponentFixture<SolicitudesPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudesPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudesPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
