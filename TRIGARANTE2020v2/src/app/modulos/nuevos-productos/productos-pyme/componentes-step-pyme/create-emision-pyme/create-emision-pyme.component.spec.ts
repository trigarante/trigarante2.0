import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEmisionPymeComponent } from './create-emision-pyme.component';

describe('CreateEmisionPymeComponent', () => {
  let component: CreateEmisionPymeComponent;
  let fixture: ComponentFixture<CreateEmisionPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEmisionPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEmisionPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
