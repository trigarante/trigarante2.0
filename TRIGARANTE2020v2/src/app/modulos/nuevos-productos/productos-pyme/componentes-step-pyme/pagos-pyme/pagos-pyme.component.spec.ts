import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagosPymeComponent } from './pagos-pyme.component';

describe('PagosPymeComponent', () => {
  let component: PagosPymeComponent;
  let fixture: ComponentFixture<PagosPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagosPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagosPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
