import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministracionPolizasPymeComponent } from './administracion-polizas-pyme.component';

describe('AdministracionPolizasPymeComponent', () => {
  let component: AdministracionPolizasPymeComponent;
  let fixture: ComponentFixture<AdministracionPolizasPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministracionPolizasPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministracionPolizasPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
