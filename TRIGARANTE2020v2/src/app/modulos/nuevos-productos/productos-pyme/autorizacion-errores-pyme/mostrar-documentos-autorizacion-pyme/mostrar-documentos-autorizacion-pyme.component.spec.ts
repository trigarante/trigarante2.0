import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarDocumentosAutorizacionPymeComponent } from './mostrar-documentos-autorizacion-pyme.component';

describe('MostrarDocumentosAutorizacionPymeComponent', () => {
  let component: MostrarDocumentosAutorizacionPymeComponent;
  let fixture: ComponentFixture<MostrarDocumentosAutorizacionPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostrarDocumentosAutorizacionPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarDocumentosAutorizacionPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
