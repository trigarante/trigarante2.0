import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentosCorreccionPymeComponent } from './documentos-correccion-pyme.component';

describe('DocumentosCorreccionPymeComponent', () => {
  let component: DocumentosCorreccionPymeComponent;
  let fixture: ComponentFixture<DocumentosCorreccionPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentosCorreccionPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentosCorreccionPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
