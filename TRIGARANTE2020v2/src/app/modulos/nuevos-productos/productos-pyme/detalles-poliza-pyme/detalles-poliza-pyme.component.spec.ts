import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesPolizaPymeComponent } from './detalles-poliza-pyme.component';

describe('DetallesPolizaPymeComponent', () => {
  let component: DetallesPolizaPymeComponent;
  let fixture: ComponentFixture<DetallesPolizaPymeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallesPolizaPymeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesPolizaPymeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
