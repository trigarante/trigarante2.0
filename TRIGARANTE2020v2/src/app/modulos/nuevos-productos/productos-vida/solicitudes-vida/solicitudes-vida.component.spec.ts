import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudesVidaComponent } from './solicitudes-vida.component';

describe('SolicitudesVidaComponent', () => {
  let component: SolicitudesVidaComponent;
  let fixture: ComponentFixture<SolicitudesVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudesVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudesVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
