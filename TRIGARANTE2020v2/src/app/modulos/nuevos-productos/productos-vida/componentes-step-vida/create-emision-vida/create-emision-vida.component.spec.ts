import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEmisionVidaComponent } from './create-emision-vida.component';

describe('CreateEmisionVidaComponent', () => {
  let component: CreateEmisionVidaComponent;
  let fixture: ComponentFixture<CreateEmisionVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEmisionVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEmisionVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
