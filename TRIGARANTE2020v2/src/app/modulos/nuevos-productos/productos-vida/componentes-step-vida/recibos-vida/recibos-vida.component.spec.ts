import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecibosVidaComponent } from './recibos-vida.component';

describe('RecibosVidaComponent', () => {
  let component: RecibosVidaComponent;
  let fixture: ComponentFixture<RecibosVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecibosVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecibosVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
