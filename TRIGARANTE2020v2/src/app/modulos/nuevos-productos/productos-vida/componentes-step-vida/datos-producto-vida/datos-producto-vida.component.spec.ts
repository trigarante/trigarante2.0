import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosProductoVidaComponent } from './datos-producto-vida.component';

describe('DatosProductoVidaComponent', () => {
  let component: DatosProductoVidaComponent;
  let fixture: ComponentFixture<DatosProductoVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosProductoVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosProductoVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
