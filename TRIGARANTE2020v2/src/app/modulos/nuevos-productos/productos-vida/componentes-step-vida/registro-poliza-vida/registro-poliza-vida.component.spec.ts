import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroPolizaVidaComponent } from './registro-poliza-vida.component';

describe('RegistroPolizaVidaComponent', () => {
  let component: RegistroPolizaVidaComponent;
  let fixture: ComponentFixture<RegistroPolizaVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroPolizaVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroPolizaVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
