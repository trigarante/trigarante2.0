import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministracionPolizasVidaComponent } from './administracion-polizas-vida.component';

describe('AdministracionPolizasVidaComponent', () => {
  let component: AdministracionPolizasVidaComponent;
  let fixture: ComponentFixture<AdministracionPolizasVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministracionPolizasVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministracionPolizasVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
