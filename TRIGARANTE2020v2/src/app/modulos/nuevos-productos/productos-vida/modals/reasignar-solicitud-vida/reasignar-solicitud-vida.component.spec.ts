import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReasignarSolicitudVidaComponent } from './reasignar-solicitud-vida.component';

describe('ReasignarSolicitudVidaComponent', () => {
  let component: ReasignarSolicitudVidaComponent;
  let fixture: ComponentFixture<ReasignarSolicitudVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReasignarSolicitudVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReasignarSolicitudVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
