import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivacionEmpleadoVidaComponent } from './activacion-empleado-vida.component';

describe('ActivacionEmpleadoVidaComponent', () => {
  let component: ActivacionEmpleadoVidaComponent;
  let fixture: ComponentFixture<ActivacionEmpleadoVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivacionEmpleadoVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivacionEmpleadoVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
