import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarDocumentosAutorizacionVidaComponent } from './mostrar-documentos-autorizacion-vida.component';

describe('MostrarDocumentosAutorizacionVidaComponent', () => {
  let component: MostrarDocumentosAutorizacionVidaComponent;
  let fixture: ComponentFixture<MostrarDocumentosAutorizacionVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostrarDocumentosAutorizacionVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarDocumentosAutorizacionVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
