import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepCotizadorVidaComponent } from './step-cotizador-vida.component';

describe('StepCotizadorVidaComponent', () => {
  let component: StepCotizadorVidaComponent;
  let fixture: ComponentFixture<StepCotizadorVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepCotizadorVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepCotizadorVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
