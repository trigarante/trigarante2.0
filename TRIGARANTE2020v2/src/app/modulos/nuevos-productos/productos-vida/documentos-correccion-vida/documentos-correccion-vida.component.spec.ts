import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentosCorreccionVidaComponent } from './documentos-correccion-vida.component';

describe('DocumentosCorreccionVidaComponent', () => {
  let component: DocumentosCorreccionVidaComponent;
  let fixture: ComponentFixture<DocumentosCorreccionVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentosCorreccionVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentosCorreccionVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
