import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CotizadorVidaComponent } from './cotizador-vida.component';

describe('CotizadorVidaComponent', () => {
  let component: CotizadorVidaComponent;
  let fixture: ComponentFixture<CotizadorVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CotizadorVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CotizadorVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
