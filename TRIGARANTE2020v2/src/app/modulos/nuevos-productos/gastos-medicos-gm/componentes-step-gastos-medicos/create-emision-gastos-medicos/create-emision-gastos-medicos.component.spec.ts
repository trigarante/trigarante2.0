import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEmisionGastosMedicosComponent } from './create-emision-gastos-medicos.component';

describe('CreateEmisionGastosMedicosComponent', () => {
  let component: CreateEmisionGastosMedicosComponent;
  let fixture: ComponentFixture<CreateEmisionGastosMedicosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEmisionGastosMedicosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEmisionGastosMedicosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
