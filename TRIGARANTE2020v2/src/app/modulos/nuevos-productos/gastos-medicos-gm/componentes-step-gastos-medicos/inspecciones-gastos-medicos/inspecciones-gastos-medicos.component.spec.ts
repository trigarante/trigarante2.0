import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspeccionesGastosMedicosComponent } from './inspecciones-gastos-medicos.component';

describe('InspeccionesGastosMedicosComponent', () => {
  let component: InspeccionesGastosMedicosComponent;
  let fixture: ComponentFixture<InspeccionesGastosMedicosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspeccionesGastosMedicosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspeccionesGastosMedicosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
