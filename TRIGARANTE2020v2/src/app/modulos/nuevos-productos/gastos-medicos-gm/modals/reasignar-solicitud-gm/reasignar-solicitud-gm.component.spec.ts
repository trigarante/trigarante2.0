import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReasignarSolicitudGmComponent } from './reasignar-solicitud-gm.component';

describe('ReasignarSolicitudGmComponent', () => {
  let component: ReasignarSolicitudGmComponent;
  let fixture: ComponentFixture<ReasignarSolicitudGmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReasignarSolicitudGmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReasignarSolicitudGmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
