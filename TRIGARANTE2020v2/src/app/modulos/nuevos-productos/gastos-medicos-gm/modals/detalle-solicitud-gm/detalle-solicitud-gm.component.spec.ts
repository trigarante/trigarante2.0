import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleSolicitudGmComponent } from './detalle-solicitud-gm.component';

describe('DetalleSolicitudGmComponent', () => {
  let component: DetalleSolicitudGmComponent;
  let fixture: ComponentFixture<DetalleSolicitudGmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleSolicitudGmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleSolicitudGmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
