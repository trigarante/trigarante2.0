import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtiquetaSolicitudGmComponent } from './etiqueta-solicitud-gm.component';

describe('EtiquetaSolicitudGmComponent', () => {
  let component: EtiquetaSolicitudGmComponent;
  let fixture: ComponentFixture<EtiquetaSolicitudGmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtiquetaSolicitudGmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtiquetaSolicitudGmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
