import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagosGmComponent } from './pagos-gm.component';

describe('PagosGmComponent', () => {
  let component: PagosGmComponent;
  let fixture: ComponentFixture<PagosGmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagosGmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagosGmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
