import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecibosGmComponent } from './recibos-gm.component';

describe('RecibosGmComponent', () => {
  let component: RecibosGmComponent;
  let fixture: ComponentFixture<RecibosGmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecibosGmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecibosGmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
