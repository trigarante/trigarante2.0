import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministracionPolizasGmComponent } from './administracion-polizas-gm.component';

describe('AdministracionPolizasGmComponent', () => {
  let component: AdministracionPolizasGmComponent;
  let fixture: ComponentFixture<AdministracionPolizasGmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministracionPolizasGmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministracionPolizasGmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
