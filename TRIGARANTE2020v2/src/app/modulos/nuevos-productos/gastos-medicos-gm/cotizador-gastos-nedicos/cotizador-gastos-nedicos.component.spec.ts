import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CotizadorGastosNedicosComponent } from './cotizador-gastos-nedicos.component';

describe('CotizadorGastosNedicosComponent', () => {
  let component: CotizadorGastosNedicosComponent;
  let fixture: ComponentFixture<CotizadorGastosNedicosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CotizadorGastosNedicosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CotizadorGastosNedicosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
