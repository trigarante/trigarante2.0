import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearAutorizacionGnmComponent } from './crear-autorizacion-gnm.component';

describe('CrearAutorizacionGnmComponent', () => {
  let component: CrearAutorizacionGnmComponent;
  let fixture: ComponentFixture<CrearAutorizacionGnmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearAutorizacionGnmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearAutorizacionGnmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
