import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarDocumentosAutorizacionGmComponent } from './mostrar-documentos-autorizacion-gm.component';

describe('MostrarDocumentosAutorizacionGmComponent', () => {
  let component: MostrarDocumentosAutorizacionGmComponent;
  let fixture: ComponentFixture<MostrarDocumentosAutorizacionGmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostrarDocumentosAutorizacionGmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarDocumentosAutorizacionGmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
