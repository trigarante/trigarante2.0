import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivacionEmpleadoGmComponent } from './activacion-empleado-gm.component';

describe('ActivacionEmpleadoGmComponent', () => {
  let component: ActivacionEmpleadoGmComponent;
  let fixture: ComponentFixture<ActivacionEmpleadoGmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivacionEmpleadoGmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivacionEmpleadoGmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
