import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorrecionErroresGmComponent } from './correcion-errores-gm.component';

describe('CorrecionErroresGmComponent', () => {
  let component: CorrecionErroresGmComponent;
  let fixture: ComponentFixture<CorrecionErroresGmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorrecionErroresGmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorrecionErroresGmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
