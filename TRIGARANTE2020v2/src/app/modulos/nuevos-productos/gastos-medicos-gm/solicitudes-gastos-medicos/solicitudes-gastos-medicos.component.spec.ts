import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudesGastosMedicosComponent } from './solicitudes-gastos-medicos.component';

describe('SolicitudesGastosMedicosComponent', () => {
  let component: SolicitudesGastosMedicosComponent;
  let fixture: ComponentFixture<SolicitudesGastosMedicosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudesGastosMedicosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudesGastosMedicosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
