import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolizaRecibosHogarComponent } from './poliza-recibos-hogar.component';

describe('PolizaRecibosHogarComponent', () => {
  let component: PolizaRecibosHogarComponent;
  let fixture: ComponentFixture<PolizaRecibosHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolizaRecibosHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolizaRecibosHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
