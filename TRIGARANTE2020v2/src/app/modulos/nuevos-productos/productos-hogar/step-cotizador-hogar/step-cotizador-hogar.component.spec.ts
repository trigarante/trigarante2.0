import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepCotizadorHogarComponent } from './step-cotizador-hogar.component';

describe('StepCotizadorHogarComponent', () => {
  let component: StepCotizadorHogarComponent;
  let fixture: ComponentFixture<StepCotizadorHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepCotizadorHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepCotizadorHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
