import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CotizadorHogarComponent } from './cotizador-hogar.component';

describe('CotizadorHogarComponent', () => {
  let component: CotizadorHogarComponent;
  let fixture: ComponentFixture<CotizadorHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CotizadorHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CotizadorHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
