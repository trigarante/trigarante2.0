import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorizacionesProcesoHogarComponent } from './autorizaciones-proceso-hogar.component';

describe('AutorizacionesProcesoHogarComponent', () => {
  let component: AutorizacionesProcesoHogarComponent;
  let fixture: ComponentFixture<AutorizacionesProcesoHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutorizacionesProcesoHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorizacionesProcesoHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
