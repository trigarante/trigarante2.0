import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecibosHogarComponent } from './recibos-hogar.component';

describe('RecibosHogarComponent', () => {
  let component: RecibosHogarComponent;
  let fixture: ComponentFixture<RecibosHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecibosHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecibosHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
