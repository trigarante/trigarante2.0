import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspeccionesHogarComponent } from './inspecciones-hogar.component';

describe('InspeccionesHogarComponent', () => {
  let component: InspeccionesHogarComponent;
  let fixture: ComponentFixture<InspeccionesHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspeccionesHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspeccionesHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
