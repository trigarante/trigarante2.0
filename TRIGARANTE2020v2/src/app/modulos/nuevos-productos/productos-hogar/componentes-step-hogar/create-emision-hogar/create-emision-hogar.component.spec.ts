import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEmisionHogarComponent } from './create-emision-hogar.component';

describe('CreateEmisionHogarComponent', () => {
  let component: CreateEmisionHogarComponent;
  let fixture: ComponentFixture<CreateEmisionHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEmisionHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEmisionHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
