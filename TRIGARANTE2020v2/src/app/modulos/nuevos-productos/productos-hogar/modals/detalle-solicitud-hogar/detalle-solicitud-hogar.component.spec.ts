import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleSolicitudHogarComponent } from './detalle-solicitud-hogar.component';

describe('DetalleSolicitudHogarComponent', () => {
  let component: DetalleSolicitudHogarComponent;
  let fixture: ComponentFixture<DetalleSolicitudHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleSolicitudHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleSolicitudHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
