import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReasignarSolicitudHogarComponent } from './reasignar-solicitud-hogar.component';

describe('ReasignarSolicitudHogarComponent', () => {
  let component: ReasignarSolicitudHogarComponent;
  let fixture: ComponentFixture<ReasignarSolicitudHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReasignarSolicitudHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReasignarSolicitudHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
