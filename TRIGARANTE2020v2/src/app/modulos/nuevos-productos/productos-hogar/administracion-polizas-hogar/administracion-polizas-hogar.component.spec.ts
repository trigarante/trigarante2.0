import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministracionPolizasHogarComponent } from './administracion-polizas-hogar.component';

describe('AdministracionPolizasHogarComponent', () => {
  let component: AdministracionPolizasHogarComponent;
  let fixture: ComponentFixture<AdministracionPolizasHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministracionPolizasHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministracionPolizasHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
