import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivacionEmpleadoHogarComponent } from './activacion-empleado-hogar.component';

describe('ActivacionEmpleadoHogarComponent', () => {
  let component: ActivacionEmpleadoHogarComponent;
  let fixture: ComponentFixture<ActivacionEmpleadoHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivacionEmpleadoHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivacionEmpleadoHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
