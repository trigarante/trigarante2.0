import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BajaRecibosComponent } from './baja-recibos.component';

describe('BajaRecibosComponent', () => {
  let component: BajaRecibosComponent;
  let fixture: ComponentFixture<BajaRecibosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BajaRecibosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BajaRecibosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
