import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MensajesTablaComponent } from './mensajes-tabla.component';

describe('MensajesTablaComponent', () => {
  let component: MensajesTablaComponent;
  let fixture: ComponentFixture<MensajesTablaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensajesTablaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MensajesTablaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
